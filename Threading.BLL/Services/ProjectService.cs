using System;
using System.Linq;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using AutoMapper;
using FluentValidation;
using Threading.Common.Extensions;
using Threading.Common.DTO.Task;
using Threading.Common.DTO.Project;
using Threading.Common.Exceptions;
using Threading.DAL.Entities;
using Threading.BLL.Interfaces;
using Threading.DAL.Context;
using System.Threading;

namespace Threading.BLL.Services
{
    public class ProjectService
        : AbstractService<Project, ListProjectDTO, ProjectOptionDTO, ProjectDTO, CreateProjectDTO, UpdateProjectDTO>, IProjectService
    {
        private readonly ITeamService _teamService;
        private readonly IUserService _userService;

        public ProjectService(
            DatabaseContext context,
            IMapper mapper,
            IValidator<CreateProjectDTO> createValidator,
            IValidator<UpdateProjectDTO> updateValidator,
            ITeamService teamService,
            IUserService userService
        ) : base(context, mapper, createValidator, updateValidator, ctx => ctx.Projects)
        {
            _teamService = teamService;
            _userService = userService;
        }

        public async Task<List<ProjectWithTasksCountDTO>> GetWithTasksCountAsync(int userId)
        {
            bool userExists = await _userService.ExistsAsync(userId);

            if (!userExists)
            {
                throw new NotFoundException("User");
            }

            // Tasks count is handled by mapper (it is simple enough)

            return await IncludeRelations(_context.Projects)
                .Where(p => p.AuthorId == userId)
                .Select(_mapper.Map<Project, ProjectWithTasksCountDTO>)
                .ToListAsync();
        }

        public async Task<ProjectAdditionalInfoDTO> GetAdditionalInfoAsync(int id)
        {
            // This splitting increases performance in 8 and more times
            Project project = await IncludeRelations(_context.Projects)
                .Where(p => p.Id == id)
                .FirstOrDefaultAsync();

            if (project == null)
            {
                throw new NotFoundException();
            }

            List<Task<dynamic>> tasks = await RunParallelQueries(
                async ctx =>
                {
                    TaskModel task = await ctx.Tasks
                        .Where(t => t.ProjectId == id)
                        .Include(t => t.Performer)
                        .OrderBy(t => t.Name.Length)
                        .FirstOrDefaultAsync();

                    return task;
                },
                async ctx =>
                {
                    TaskModel task = await _context.Tasks
                        .Where(t => t.ProjectId == id)
                        .Include(t => t.Performer)
                        .OrderByDescending(t => t.Description.Length)
                        .FirstOrDefaultAsync();

                    return task;
                }
            );

            TaskModel shortestTask = tasks[0].Result; // Just result accessing, not waiting
            TaskModel longestTask = tasks[1].Result;

            return new ProjectAdditionalInfoDTO
            {
                Project = _mapper.Map<Project, ProjectDTO>(project),
                ShortestNameTask = shortestTask != null ? _mapper.Map<TaskModel, TaskDTO>(shortestTask) : null,
                LongestDescriptionTask = longestTask != null ? _mapper.Map<TaskModel, TaskDTO>(longestTask) : null,
                UsersCountInTeam = project.Description.Length > 20 || project.Tasks.Count() < 3
                    ? project.Team.Users.Count()
                    : null
            };
        }

        public Task<List<ProjectWithTasksCountDTO>> GetWithTasksCountAsync(int userId, CancellationToken cancellationToken)
        {
            return Task.Run(async () => await GetWithTasksCountAsync(userId), cancellationToken);
        }

        public Task<ProjectAdditionalInfoDTO> GetAdditionalInfoAsync(int id, CancellationToken cancellationToken)
        {
            return Task.Run(async () => await GetAdditionalInfoAsync(id), cancellationToken);
        }

        protected override IQueryable<Project> IncludeRelations(IQueryable<Project> queryable)
        {
            return queryable
                .Include(p => p.Author)
                .Include(p => p.Team)
                    .ThenInclude(t => t.Users)
                .Include(p => p.Tasks)
                    .ThenInclude(t => t.Performer);
        }

        protected override IQueryable<Project> IncludeListRelations(IQueryable<Project> queryable)
        {
            return queryable
                .Include(p => p.Author)
                .Include(p => p.Team);
        }

        protected override async Task CheckRelations(Project entity)
        {
            if (entity.TeamId != null)
            {
                bool teamExists = await _teamService.ExistsAsync(entity.TeamId.Value);

                if (!teamExists)
                {
                    throw new NotFoundException("Team");
                }
            }

            if (entity.AuthorId != null)
            {
                bool authorExists = await _userService.ExistsAsync(entity.AuthorId.Value);

                if (!authorExists)
                {
                    throw new NotFoundException("Author");
                }
            }
        }
    }
}
