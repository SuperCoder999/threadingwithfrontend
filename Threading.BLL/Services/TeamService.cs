using System;
using System.Linq;
using System.Threading.Tasks;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using AutoMapper;
using FluentValidation;
using Threading.Common.Extensions;
using Threading.Common.DTO.Team;
using Threading.DAL.Entities;
using Threading.BLL.Interfaces;
using Threading.DAL.Context;
using System.Threading;

namespace Threading.BLL.Services
{
    public class TeamService
        : AbstractService<Team, ListTeamDTO, TeamOptionDTO, TeamDTO, CreateTeamDTO, UpdateTeamDTO>, ITeamService
    {
        public TeamService(
            DatabaseContext context,
            IMapper mapper,
            IValidator<CreateTeamDTO> createValidator,
            IValidator<UpdateTeamDTO> updateValidator
        ) : base(context, mapper, createValidator, updateValidator, ctx => ctx.Teams) { }

        public async Task<List<TeamShortDTO>> GetShortWithUsersOlder10Async()
        {
            return await IncludeRelations(_context.Teams)
                .Where(t => t.Users.All(u => DateTime.UtcNow.Year - u.BirthDay.Year > 10) && t.Users.Count() > 0)
                .Select(t => new Team
                {
                    Id = t.Id,
                    Name = t.Name,
                    CreatedAt = t.CreatedAt,
                    Users = t.Users.OrderByDescending(u => u.RegisteredAt).AsEnumerable()
                })
                .Select(_mapper.Map<Team, TeamShortDTO>)
                .ToListAsync();
        }

        public Task<List<TeamShortDTO>> GetShortWithUsersOlder10Async(CancellationToken cancellationToken)
        {
            return Task.Run(async () => await GetShortWithUsersOlder10Async(), cancellationToken);
        }

        protected override IQueryable<Team> IncludeRelations(IQueryable<Team> queryable)
        {
            return queryable.Include(t => t.Users);
        }
    }
}
