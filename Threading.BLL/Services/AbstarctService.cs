using System;
using System.Linq;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using AutoMapper;
using FluentValidation;
using Newtonsoft.Json;
using Threading.DAL.Context;
using Threading.DAL.Entities;
using Threading.BLL.Interfaces;
using Threading.Common;
using Threading.Common.Extensions;
using Threading.Common.Exceptions;

namespace Threading.BLL.Services
{
    public abstract class AbstractService<TEntity, TListDTO, TOptionDTO, TDTO, TCreateDTO, TUpdateDTO>
        : IService<TEntity, TListDTO, TOptionDTO, TDTO, TCreateDTO>
        where TEntity : class, IEntity, new()
        where TDTO : struct
        where TCreateDTO : struct
    {
        protected readonly DatabaseContext _context;
        protected readonly IMapper _mapper;
        protected readonly IValidator<TCreateDTO> _createValidator;
        protected readonly IValidator<TUpdateDTO> _updateValidator;
        private readonly Func<DbSet<TEntity>> _GetDBSet;

        public AbstractService(
            DatabaseContext context,
            IMapper mapper,
            IValidator<TCreateDTO> createValidator,
            IValidator<TUpdateDTO> updateValidator,
            Func<DatabaseContext, DbSet<TEntity>> DBSetSelector
        )
        {
            _context = context;
            _mapper = mapper;
            _createValidator = createValidator;
            _updateValidator = updateValidator;
            _GetDBSet = () => DBSetSelector(context);
        }

        public virtual async Task<List<TListDTO>> GetAsync()
        {
            return await IncludeRelations(_GetDBSet())
                .Select(_mapper.Map<TEntity, TListDTO>)
                .ToListAsync();
        }

        public virtual async Task<TDTO> GetAsync(int id)
        {
            TEntity entity = await IncludeRelations(_GetDBSet())
                .Where(e => e.Id == id)
                .FirstOrDefaultAsync();

            if (entity == null)
            {
                throw new NotFoundException();
            }

            return _mapper.Map<TEntity, TDTO>(entity);
        }

        public virtual async Task<List<TOptionDTO>> GetOptionsAsync()
        {
            return await _GetDBSet()
                .Select(_mapper.Map<TEntity, TOptionDTO>)
                .ToListAsync();
        }

        public virtual async Task<TEntity> GetRawAsync(int id)
        {
            TEntity entity = await _GetDBSet()
                .Where(e => e.Id == id)
                .FirstOrDefaultAsync();

            if (entity == null)
            {
                throw new NotFoundException();
            }

            return entity;
        }

        public virtual async Task<bool> ExistsAsync(int id)
        {
            return await _GetDBSet()
                .AnyAsync(e => e.Id == id);
        }

        public virtual async Task<int> CreateAsync(TCreateDTO data)
        {
            await _createValidator.ValidateAndThrowAsync(data);
            TEntity entity = _mapper.Map<TCreateDTO, TEntity>(data);

            await CheckRelations(entity);
            await _GetDBSet().AddAsync(entity);
            await _context.SaveChangesAsync();

            return entity.Id;
        }

        public virtual async Task UpdateAsync(int id, TEntity data)
        //                                             ^^^^^^^ Custom logic
        {
            bool exists = await ExistsAsync(id);

            if (!exists)
            {
                throw new NotFoundException();
            }

            data.Id = id;
            await CheckRelations(data);

            _GetDBSet().Update(data);
            await _context.SaveChangesAsync();
        }

        public virtual async Task<TEntity> RepopulateUpdateRequestAsync(string stringBody, TEntity existing)
        {
            TUpdateDTO oldAsDTO = _mapper.Map<TEntity, TUpdateDTO>(existing);
            object oldAsObject = (object)oldAsDTO;

            JsonConvert.PopulateObject(stringBody, oldAsObject);
            TUpdateDTO newAsDTO = (TUpdateDTO)oldAsObject;

            await _updateValidator.ValidateAndThrowAsync(newAsDTO);
            Utils.PopulateCSharpObject(newAsDTO, ref existing);

            return existing;
        }

        public virtual async Task DeleteAsync(int id)
        {
            bool exists = await ExistsAsync(id);

            if (!exists)
            {
                throw new NotFoundException();
            }

            _GetDBSet().Remove(new TEntity { Id = id });
            await _context.SaveChangesAsync();
        }

        public Task<List<TListDTO>> GetAsync(CancellationToken cancellationToken)
        {
            return Task.Run(async () => await GetAsync(), cancellationToken);
        }

        public Task<TDTO> GetAsync(int id, CancellationToken cancellationToken)
        {
            return Task.Run(async () => await GetAsync(id), cancellationToken);
        }

        public Task<List<TOptionDTO>> GetOptionsAsync(CancellationToken cancellationToken)
        {
            return Task.Run(async () => await GetOptionsAsync(), cancellationToken);
        }

        public Task<TEntity> GetRawAsync(int id, CancellationToken cancellationToken)
        {
            return Task.Run(async () => await GetRawAsync(id), cancellationToken);
        }

        public Task<bool> ExistsAsync(int id, CancellationToken cancellationToken)
        {
            return Task.Run(async () => await ExistsAsync(id), cancellationToken);
        }

        public Task<int> CreateAsync(TCreateDTO data, CancellationToken cancellationToken)
        {
            return Task.Run(async () => await CreateAsync(data), cancellationToken);
        }

        public Task UpdateAsync(int id, TEntity data, CancellationToken cancellationToken)
        {
            return Task.Run(async () => await UpdateAsync(id, data), cancellationToken);
        }

        public Task<TEntity> RepopulateUpdateRequestAsync(string stringBody, TEntity existing, CancellationToken cancellationToken)
        {
            return Task.Run(async () => await RepopulateUpdateRequestAsync(stringBody, existing), cancellationToken);
        }

        public Task DeleteAsync(int id, CancellationToken cancellationToken)
        {
            return Task.Run(async () => await DeleteAsync(id), cancellationToken);
        }

        protected virtual async Task<List<Task<dynamic>>> RunParallelQueries(params Func<DatabaseContext, Task<object>>[] queries)
        {
            List<DatabaseContext> contexts = new List<DatabaseContext>();

            for (int i = 0; i < queries.Length; i++)
            {
                contexts.Add(new DatabaseContext(_context.Options));
            }

            List<Task<dynamic>> tasks = queries.Select((query, index) => query(contexts[index])).ToList();
            await Task.WhenAll(tasks);

            return tasks;
        }

        protected virtual IQueryable<TEntity> IncludeRelations(IQueryable<TEntity> queryable) => IncludeListRelations(queryable);
        protected virtual IQueryable<TEntity> IncludeListRelations(IQueryable<TEntity> queryable) => queryable;
        protected virtual Task CheckRelations(TEntity entity) => Task.Run(() => { });
    }
}
