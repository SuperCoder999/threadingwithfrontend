using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using AutoMapper;
using FluentValidation;
using Threading.Common.Extensions;
using Threading.Common.DTO.User;
using Threading.Common.DTO.Task;
using Threading.Common.DTO.Project;
using Threading.Common.Exceptions;
using Threading.DAL.Entities;
using Threading.BLL.Interfaces;
using Threading.DAL.Context;
using System.Collections.Generic;
using System.Threading;

namespace Threading.BLL.Services
{
    public class UserService
        : AbstractService<User, UserDTO, UserOptionDTO, UserDTO, CreateUserDTO, UpdateUserDTO>, IUserService
    {
        private readonly ITeamService _teamService;

        public UserService(
            DatabaseContext context,
            IMapper mapper,
            IValidator<CreateUserDTO> createValidator,
            IValidator<UpdateUserDTO> updateValidator,
            ITeamService teamService
        ) : base(context, mapper, createValidator, updateValidator, ctx => ctx.Users)
        {
            _teamService = teamService;
        }

        public async Task<List<UserWithTasksDTO>> GetWithTasksOrderedByFirstNameAsync()
        {
            return await _context.Users
                .AsNoTracking()
                .Include(u => u.Tasks.OrderByDescending(t => t.Name.Length))
                .OrderBy(u => u.FirstName)
                .Select(_mapper.Map<User, UserWithTasksDTO>)
                .ToListAsync();
        }

        public async Task<UserAdditionalInfoDTO> GetAdditionalInfoAsync(int id)
        {
            // This splitting increases performance in 8 and more times
            User user = await _context.Users
                .Where(u => u.Id == id)
                .FirstOrDefaultAsync();

            if (user == null)
            {
                throw new NotFoundException();
            }

            List<Task<dynamic>> tasks = await RunParallelQueries(
                async ctx =>
                {
                    Project project = await ctx.Projects
                        .AsSplitQuery()
                        .Where(p => p.AuthorId == id)
                        .Include(p => p.Team)
                            .ThenInclude(t => t.Users)
                        .Include(p => p.Tasks)
                            .ThenInclude(t => t.Performer)
                        .OrderByDescending(p => p.CreatedAt)
                        .FirstOrDefaultAsync();

                    return project;
                },
                async ctx =>
                {
                    TaskModel task = await ctx.Tasks
                        .Where(t => t.PerformerId == id && t.FinishedAt.HasValue)
                        .OrderByDescending(t => t.FinishedAt - t.CreatedAt)
                        .FirstOrDefaultAsync();

                    return task;
                }
            );

            Project lastProject = tasks[0].Result; // Just result accessing, not waiting
            TaskModel longestTask = tasks[1].Result;

            return new UserAdditionalInfoDTO
            {
                User = _mapper.Map<User, UserDTO>(user),
                LastProject = lastProject != null ? _mapper.Map<Project, ProjectDTO>(lastProject) : null,
                LastProjectTasksCount = lastProject != null ? lastProject.Tasks.Count() : 0,
                UnfinishedTasksCount = _context.Tasks
                    .Where(t => t.PerformerId == id && !t.FinishedAt.HasValue)
                    .Count(),
                LongestTask = longestTask != null ? _mapper.Map<TaskModel, TaskWithoutPerformerDTO>(longestTask) : null
            };
        }

        public Task<List<UserWithTasksDTO>> GetWithTasksOrderedByFirstNameAsync(CancellationToken cancellationToken)
        {
            return Task.Run(async () => await GetWithTasksOrderedByFirstNameAsync(), cancellationToken);
        }

        public Task<UserAdditionalInfoDTO> GetAdditionalInfoAsync(int id, CancellationToken cancellationToken)
        {
            return Task.Run(async () => await GetAdditionalInfoAsync(id), cancellationToken);
        }

        protected override async Task CheckRelations(User entity)
        {
            if (entity.TeamId != null)
            {
                bool teamExists = await _teamService.ExistsAsync(entity.TeamId.Value);

                if (!teamExists)
                {
                    throw new NotFoundException("Team");
                }
            }
        }
    }
}
