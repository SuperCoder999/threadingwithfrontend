using System;
using System.Linq;
using System.Threading.Tasks;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using AutoMapper;
using FluentValidation;
using Threading.Common.Extensions;
using Threading.Common.DTO.Task;
using Threading.Common.Exceptions;
using Threading.DAL.Entities;
using Threading.BLL.Interfaces;
using Threading.DAL.Context;
using System.Threading;

namespace Threading.BLL.Services
{
    public class TaskService
        : AbstractService<TaskModel, TaskDTO, TaskOptionDTO, TaskDTO, CreateTaskDTO, UpdateTaskDTO>, ITaskService
    {
        private readonly IProjectService _projectService;
        private readonly IUserService _userService;

        public TaskService(
            DatabaseContext context,
            IMapper mapper,
            IValidator<CreateTaskDTO> createValidator,
            IValidator<UpdateTaskDTO> updateValidator,
            IProjectService projectService,
            IUserService userService
        ) : base(context, mapper, createValidator, updateValidator, ctx => ctx.Tasks)
        {
            _projectService = projectService;
            _userService = userService;
        }

        public async Task<List<TaskDTO>> GetAssignedToWithShortNameAsync(int userId)
        {
            bool userExists = await _userService.ExistsAsync(userId);

            if (!userExists)
            {
                throw new NotFoundException("User");
            }

            return await IncludeRelations(_context.Tasks)
                .Where(t => t.Name.Length < 45 && t.PerformerId == userId)
                .Select(_mapper.Map<TaskModel, TaskDTO>)
                .ToListAsync();
        }

        public async Task<List<TaskShortDTO>> GetShortFinishedInCurrentYearAssignedToAsync(int userId)
        {
            bool userExists = await _userService.ExistsAsync(userId);

            if (!userExists)
            {
                throw new NotFoundException("User");
            }

            return await _context.Tasks
                .Where(t =>
                    t.FinishedAt.HasValue &&
                    t.FinishedAt.Value.Year == DateTime.UtcNow.Year &&
                    t.PerformerId == userId)
                .Select(_mapper.Map<TaskModel, TaskShortDTO>)
                .ToListAsync();
        }

        public async Task<List<TaskWithoutPerformerDTO>> GetUnfinishedForAsync(int userId)
        {
            bool userExists = await _userService.ExistsAsync(userId);

            if (!userExists)
            {
                throw new NotFoundException("User");
            }

            return await _context.Tasks
                .Where(t => t.PerformerId == userId && !t.FinishedAt.HasValue)
                .Select(_mapper.Map<TaskModel, TaskWithoutPerformerDTO>)
                .ToListAsync();
        }

        public async Task<List<TaskDTO>> GetUnfinishedAsync()
        {
            return await IncludeRelations(_context.Tasks)
                .Where(t => !t.FinishedAt.HasValue)
                .Select(_mapper.Map<TaskModel, TaskDTO>)
                .ToListAsync();
        }

        public Task<List<TaskDTO>> GetAssignedToWithShortNameAsync(int userId, CancellationToken cancellationToken)
        {
            return Task.Run(async () => await GetAssignedToWithShortNameAsync(userId), cancellationToken);
        }

        public Task<List<TaskShortDTO>> GetShortFinishedInCurrentYearAssignedToAsync(int userId, CancellationToken cancellationToken)
        {
            return Task.Run(async () => await GetShortFinishedInCurrentYearAssignedToAsync(userId), cancellationToken);
        }

        public Task<List<TaskWithoutPerformerDTO>> GetUnfinishedForAsync(int userId, CancellationToken cancellationToken)
        {
            return Task.Run(async () => await GetUnfinishedForAsync(userId), cancellationToken);
        }

        public Task<List<TaskDTO>> GetUnfinishedAsync(CancellationToken cancellationToken)
        {
            return Task.Run(async () => await GetUnfinishedAsync(), cancellationToken);
        }

        protected override IQueryable<TaskModel> IncludeListRelations(IQueryable<TaskModel> queryable)
        {
            return queryable.Include(t => t.Performer);
        }

        protected override async Task CheckRelations(TaskModel entity)
        {
            bool projectExists = await _projectService.ExistsAsync(entity.ProjectId);

            if (!projectExists)
            {
                throw new NotFoundException("Project");
            }

            if (entity.PerformerId != null)
            {
                bool performerExists = await _userService.ExistsAsync(entity.PerformerId.Value);

                if (!performerExists)
                {
                    throw new NotFoundException("Performer");
                }
            }
        }
    }
}
