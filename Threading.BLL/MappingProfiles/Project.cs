using System.Linq;
using AutoMapper;
using Threading.DAL.Entities;
using Threading.Common.DTO.Project;

namespace Threading.BLL.MappingProfiles
{
    public sealed class ProjectProfile : Profile
    {
        public ProjectProfile()
        {
            CreateMap<CreateProjectDTO, Project>();
            CreateMap<Project, UpdateProjectDTO>();
            CreateMap<Project, ProjectDTO>();
            CreateMap<Project, ListProjectDTO>();
            CreateMap<Project, ProjectOptionDTO>();

            CreateMap<Project, ProjectWithTasksCountDTO>()
                .ForMember<int>(dto => dto.TasksCount, opt => opt.MapFrom(p => p.Tasks.Count()));
        }
    }
}
