using AutoMapper;
using Threading.DAL.Entities;
using Threading.Common.DTO.Team;

namespace Threading.BLL.MappingProfiles
{
    public sealed class TeamProfile : Profile
    {
        public TeamProfile()
        {
            CreateMap<CreateTeamDTO, Team>();
            CreateMap<Team, UpdateTeamDTO>();
            CreateMap<Team, TeamDTO>();
            CreateMap<Team, ListTeamDTO>();
            CreateMap<Team, TeamShortDTO>();
            CreateMap<Team, TeamOptionDTO>();
        }
    }
}
