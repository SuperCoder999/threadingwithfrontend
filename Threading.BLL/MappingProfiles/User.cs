using AutoMapper;
using Threading.DAL.Entities;
using Threading.Common.DTO.User;

namespace Threading.BLL.MappingProfiles
{
    public sealed class UserProfile : Profile
    {
        public UserProfile()
        {
            CreateMap<CreateUserDTO, User>();
            CreateMap<User, UpdateUserDTO>();
            CreateMap<User, UserDTO>();
            CreateMap<User, UserWithTasksDTO>();

            CreateMap<User, UserOptionDTO>()
                .ForMember(dto => dto.Name, opt => opt.MapFrom(u =>
                    u.FirstName != null && u.LastName != null && u.FirstName != "" && u.LastName != ""
                        ? u.FirstName + " " + u.LastName
                        : u.Email));
        }
    }
}
