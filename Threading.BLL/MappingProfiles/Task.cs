using AutoMapper;
using Threading.DAL.Entities;
using Threading.Common.DTO.Task;

namespace Threading.BLL.MappingProfiles
{
    public sealed class TaskProfile : Profile
    {
        public TaskProfile()
        {
            CreateMap<CreateTaskDTO, TaskModel>();
            CreateMap<TaskModel, UpdateTaskDTO>();
            CreateMap<TaskModel, TaskDTO>();
            CreateMap<TaskModel, TaskWithoutPerformerDTO>();
            CreateMap<TaskModel, TaskShortDTO>();
            CreateMap<TaskModel, TaskOptionDTO>();
        }
    }
}
