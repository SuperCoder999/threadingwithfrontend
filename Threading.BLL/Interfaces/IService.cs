using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Threading.DAL.Entities;

namespace Threading.BLL.Interfaces
{
    public interface IService<TEntity, TListDTO, TOptionDTO, TDTO, TCreateDTO>
        where TEntity : class, IEntity, new()
        where TDTO : struct
        where TCreateDTO : struct
    {
        Task<List<TListDTO>> GetAsync();
        Task<TDTO> GetAsync(int id);
        Task<List<TOptionDTO>> GetOptionsAsync();
        Task<TEntity> GetRawAsync(int id);
        Task<bool> ExistsAsync(int id);
        Task<int> CreateAsync(TCreateDTO data);
        Task UpdateAsync(int id, TEntity data);
        Task<TEntity> RepopulateUpdateRequestAsync(string stringBody, TEntity existing);
        Task DeleteAsync(int id);

        Task<List<TListDTO>> GetAsync(CancellationToken cancellationToken);
        Task<TDTO> GetAsync(int id, CancellationToken cancellationToken);
        Task<List<TOptionDTO>> GetOptionsAsync(CancellationToken cancellationToken);
        Task<TEntity> GetRawAsync(int id, CancellationToken cancellationToken);
        Task<bool> ExistsAsync(int id, CancellationToken cancellationToken);
        Task<int> CreateAsync(TCreateDTO data, CancellationToken cancellationToken);
        Task UpdateAsync(int id, TEntity data, CancellationToken cancellationToken);
        Task<TEntity> RepopulateUpdateRequestAsync(string stringBody, TEntity existing, CancellationToken cancellationToken);
        Task DeleteAsync(int id, CancellationToken cancellationToken);
    }
}
