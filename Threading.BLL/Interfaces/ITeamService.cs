using System.Threading;
using System.Threading.Tasks;
using System.Collections.Generic;
using Threading.Common.DTO.Team;
using Threading.DAL.Entities;

namespace Threading.BLL.Interfaces
{
    public interface ITeamService : IService<Team, ListTeamDTO, TeamOptionDTO, TeamDTO, CreateTeamDTO>
    {
        Task<List<TeamShortDTO>> GetShortWithUsersOlder10Async();

        Task<List<TeamShortDTO>> GetShortWithUsersOlder10Async(CancellationToken cancellationToken);
    }
}
