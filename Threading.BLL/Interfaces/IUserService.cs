using System.Threading;
using System.Threading.Tasks;
using System.Collections.Generic;
using Threading.Common.DTO.User;
using Threading.DAL.Entities;

namespace Threading.BLL.Interfaces
{
    public interface IUserService : IService<User, UserDTO, UserOptionDTO, UserDTO, CreateUserDTO>
    {
        Task<List<UserWithTasksDTO>> GetWithTasksOrderedByFirstNameAsync();
        Task<UserAdditionalInfoDTO> GetAdditionalInfoAsync(int id);

        Task<List<UserWithTasksDTO>> GetWithTasksOrderedByFirstNameAsync(CancellationToken cancellationToken);
        Task<UserAdditionalInfoDTO> GetAdditionalInfoAsync(int id, CancellationToken cancellationToken);
    }
}
