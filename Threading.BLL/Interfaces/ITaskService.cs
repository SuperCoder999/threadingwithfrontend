using System.Threading;
using System.Threading.Tasks;
using System.Collections.Generic;
using Threading.Common.DTO.Task;
using Threading.DAL.Entities;

namespace Threading.BLL.Interfaces
{
    public interface ITaskService : IService<TaskModel, TaskDTO, TaskOptionDTO, TaskDTO, CreateTaskDTO>
    {
        Task<List<TaskDTO>> GetAssignedToWithShortNameAsync(int userId);
        Task<List<TaskShortDTO>> GetShortFinishedInCurrentYearAssignedToAsync(int userId);
        Task<List<TaskWithoutPerformerDTO>> GetUnfinishedForAsync(int userId);
        Task<List<TaskDTO>> GetUnfinishedAsync();

        Task<List<TaskDTO>> GetAssignedToWithShortNameAsync(int userId, CancellationToken cancellationToken);
        Task<List<TaskShortDTO>> GetShortFinishedInCurrentYearAssignedToAsync(int userId, CancellationToken cancellationToken);
        Task<List<TaskWithoutPerformerDTO>> GetUnfinishedForAsync(int userId, CancellationToken cancellationToken);
        Task<List<TaskDTO>> GetUnfinishedAsync(CancellationToken cancellationToken);
    }
}
