using System.Threading;
using System.Threading.Tasks;
using System.Collections.Generic;
using Threading.Common.DTO.Project;
using Threading.DAL.Entities;

namespace Threading.BLL.Interfaces
{
    public interface IProjectService : IService<Project, ListProjectDTO, ProjectOptionDTO, ProjectDTO, CreateProjectDTO>
    {
        Task<List<ProjectWithTasksCountDTO>> GetWithTasksCountAsync(int userId);
        Task<ProjectAdditionalInfoDTO> GetAdditionalInfoAsync(int id);

        Task<List<ProjectWithTasksCountDTO>> GetWithTasksCountAsync(int userId, CancellationToken cancellationToken);
        Task<ProjectAdditionalInfoDTO> GetAdditionalInfoAsync(int id, CancellationToken cancellationToken);
    }
}
