function dayDiff(diff: number): Date {
	const date = new Date();
	date.setDate(date.getDate() + diff);

	return date;
}

export function lastDay(): Date {
	return dayDiff(-1);
}

export function nextDay(): Date {
	return dayDiff(1);
}
