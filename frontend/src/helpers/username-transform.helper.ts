import { User } from "src/models/user";

export function formatUsername(user: User): string {
	return user.firstName && user.lastName
		? user.firstName + " " + user.lastName
		: user.email;
}
