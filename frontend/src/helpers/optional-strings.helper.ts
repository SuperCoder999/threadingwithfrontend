export function removeOptionalStrings<T>(object: T): T {
	const result: Record<string, any> = { ...object };

	Object.entries(object).forEach(([key, value]) => {
		if (typeof value === "string" && value.length === 0) {
			result[key] = undefined;
		}
	});

	return result as T;
}
