import { TaskKanbanState } from "src/models/task";

export function getTaskStateColor(state: TaskKanbanState) {
	switch (state) {
		case TaskKanbanState.InProgress:
			return "orange";
		case TaskKanbanState.Done:
			return "green";
		case TaskKanbanState.Cancelled:
			return "red";
		case TaskKanbanState.ToDo:
		default:
			return "blue";
	}
}
