import { TaskKanbanState } from "src/models/task";

export function formatTaskKanbanState(state: TaskKanbanState): string {
	switch (state) {
		case TaskKanbanState.ToDo:
			return "To Do";
		case TaskKanbanState.InProgress:
			return "In Progress";
		case TaskKanbanState.Done:
			return "Done";
		case TaskKanbanState.Cancelled:
			return "Cancelled";
		default:
			return "Unknown";
	}
}
