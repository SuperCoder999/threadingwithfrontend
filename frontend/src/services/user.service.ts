import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { BaseHttpService } from "./base.service";
import { User, UserOption, ModifyUser } from "src/models/user";

@Injectable({
	providedIn: "root",
})
export class UserService extends BaseHttpService<
	User,
	User,
	UserOption,
	ModifyUser,
	ModifyUser
> {
	public constructor(client: HttpClient) {
		super("users", client);
	}
}
