import { HttpClient } from "@angular/common/http";
import { Observable } from "rxjs";

export abstract class BaseHttpService<TList, T, TOption, TCreate, TUpdate> {
	public constructor(
		protected readonly entity: string,
		protected readonly client: HttpClient
	) {}

	public get(): Observable<TList[]> {
		return this.client.get<TList[]>(this.entity);
	}

	public getOne(id: number): Observable<T> {
		return this.client.get<T>(`${this.entity}/${id}`);
	}

	public getOptions(): Observable<TOption[]> {
		return this.client.get<TOption[]>(`${this.entity}/options`);
	}

	public create(data: TCreate): Observable<T> {
		return this.client.post<T>(this.entity, data);
	}

	public update(id: number, data: TUpdate): Observable<T> {
		return this.client.patch<T>(`${this.entity}/${id}`, data);
	}

	public delete(id: number): Observable<void> {
		return this.client.delete<void>(`${this.entity}/${id}`);
	}
}
