import { Injectable } from "@angular/core";
import { MatSnackBar } from "@angular/material/snack-bar";
import { ErrorMessage } from "src/models/errors";

@Injectable({
	providedIn: "root",
})
export class NotificationsService {
	public constructor(private readonly snackbar: MatSnackBar) {}

	public error(message: string | ErrorMessage) {
		return this.snackbar.open(message, "", {
			duration: 3000,
			panelClass: "error-snackbar",
		});
	}
}
