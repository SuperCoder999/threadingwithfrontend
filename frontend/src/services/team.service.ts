import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { BaseHttpService } from "./base.service";
import { ListTeam, Team, TeamOption, ModifyTeam } from "src/models/team";

@Injectable({
	providedIn: "root",
})
export class TeamService extends BaseHttpService<
	ListTeam,
	Team,
	TeamOption,
	ModifyTeam,
	ModifyTeam
> {
	public constructor(client: HttpClient) {
		super("teams", client);
	}
}
