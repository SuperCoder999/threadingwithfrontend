import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { BaseHttpService } from "./base.service";

import {
	ListProject,
	Project,
	ProjectOption,
	ModifyProject,
} from "src/models/project";

@Injectable({
	providedIn: "root",
})
export class ProjectService extends BaseHttpService<
	ListProject,
	Project,
	ProjectOption,
	ModifyProject,
	ModifyProject
> {
	public constructor(client: HttpClient) {
		super("projects", client);
	}
}
