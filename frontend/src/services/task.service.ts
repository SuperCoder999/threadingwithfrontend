import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { BaseHttpService } from "./base.service";
import { TaskModel, TaskOption, ModifyTask } from "src/models/task";

@Injectable({
	providedIn: "root",
})
export class TaskService extends BaseHttpService<
	TaskModel,
	TaskModel,
	TaskOption,
	ModifyTask,
	ModifyTask
> {
	public constructor(client: HttpClient) {
		super("tasks", client);
	}
}
