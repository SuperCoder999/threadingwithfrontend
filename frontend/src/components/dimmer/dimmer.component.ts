import { Component, Input } from "@angular/core";

@Component({
	selector: "app-dimmer",
	templateUrl: "./dimmer.component.html",
	styleUrls: ["./dimmer.component.scss"],
})
export class DimmerComponent {
	@Input() public open: boolean = false;
	@Input() public zIndex: number = 2;
}
