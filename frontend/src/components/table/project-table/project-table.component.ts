import { Component } from "@angular/core";
import { ListProject } from "src/models/project";
import { BasicTableComponent } from "../basic-table.component";

@Component({
	selector: "project-table",
	templateUrl: "./project-table.component.html",
	styleUrls: ["../table.component.scss"],
})
export class ProjectTableComponent extends BasicTableComponent<ListProject> {
	public override innerColumns: string[] = [
		"id",
		"name",
		"author",
		"team",
		"deadline",
		"createdAt",
	];

	public override compareData(
		a: ListProject,
		b: ListProject,
		isAsc: boolean,
		active: string
	): number {
		switch (active) {
			case "author":
				return this.compare(a.author?.id, b.author?.id, isAsc);
			case "team":
				return this.compare(a.team?.id, b.team?.id, isAsc);
			default:
				return this.compare(
					(a as any)[active],
					(b as any)[active],
					isAsc
				);
		}
	}
}
