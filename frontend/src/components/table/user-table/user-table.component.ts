import { Component } from "@angular/core";
import { User } from "src/models/user";
import { BasicTableComponent } from "../basic-table.component";

@Component({
	selector: "user-table",
	templateUrl: "./user-table.component.html",
	styleUrls: ["../table.component.scss"],
})
export class UserTableComponent extends BasicTableComponent<User> {
	public override innerColumns: string[] = [
		"id",
		"email",
		"firstName",
		"lastName",
		"team",
		"registeredAt",
		"birthDay",
	];

	public override compareData(
		a: User,
		b: User,
		isAsc: boolean,
		active: string
	): number {
		switch (active) {
			case "team":
				return this.compare(a.teamId, b.teamId, isAsc);
			default:
				return this.compare(
					(a as any)[active],
					(b as any)[active],
					isAsc
				);
		}
	}
}
