import { Component } from "@angular/core";
import { TaskModel } from "src/models/task";
import { BasicTableComponent } from "../basic-table.component";

@Component({
	selector: "task-table",
	templateUrl: "./task-table.component.html",
	styleUrls: ["../table.component.scss"],
})
export class TaskTableComponent extends BasicTableComponent<TaskModel> {
	public override innerColumns: string[] = [
		"id",
		"name",
		"project",
		"performer",
		"state",
		"createdAt",
		"finishedAt",
	];

	public override compareData(
		a: TaskModel,
		b: TaskModel,
		isAsc: boolean,
		active: string
	): number {
		switch (active) {
			case "project":
				return this.compare(a.projectId, b.projectId, isAsc);
			case "performer":
				return this.compare(a.performer?.id, b.performer?.id, isAsc);
			default:
				return this.compare(
					(a as any)[active],
					(b as any)[active],
					isAsc
				);
		}
	}
}
