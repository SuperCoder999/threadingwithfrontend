import {
	Component,
	EventEmitter,
	Input,
	OnChanges,
	OnInit,
	Output,
	SimpleChanges,
} from "@angular/core";

import { Sort } from "@angular/material/sort";

@Component({ template: "" })
export abstract class BasicTableComponent<T> implements OnInit, OnChanges {
	@Input() public data: T[] = [];
	@Input() public withActions: boolean = false;
	@Output() public onUpdate: EventEmitter<T> = new EventEmitter<T>();
	@Output() public onDelete: EventEmitter<T> = new EventEmitter<T>();

	public sortedData: T[] = [];
	public abstract innerColumns: string[];

	public ngOnChanges(changes: SimpleChanges): void {
		if (changes["data"]) {
			this.sortedData = [...this.data];
		}
	}

	public ngOnInit(): void {
		this.sortedData = [...this.data];
	}

	public emitUpdate(object: T): void {
		this.onUpdate.emit(object);
	}

	public emitDelete(object: T): void {
		this.onDelete.emit(object);
	}

	public sortData(sort: Sort): void {
		const newSortedData = [...this.data];

		if (!sort.active || sort.direction === "") {
			this.sortedData = [...newSortedData];
			return;
		}

		this.sortedData = newSortedData.sort((a, b) => {
			const isAsc = sort.direction === "asc";
			return this.compareData(a, b, isAsc, sort.active);
		});
	}

	public compare(
		a: number | string | undefined,
		b: number | string | undefined,
		isAsc: boolean
	): number {
		if (b === undefined) {
			return 1;
		}

		if (a === undefined) {
			return -1;
		}

		const difference = a < b ? -1 : 1;
		const multiplier = isAsc ? 1 : -1;

		return difference * multiplier;
	}

	public get columns(): string[] {
		return [...this.innerColumns, ...(this.withActions ? ["actions"] : [])];
	}

	public abstract compareData(
		a: T,
		b: T,
		isAsc: boolean,
		active: string
	): number;
}
