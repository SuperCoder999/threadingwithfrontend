import { Component } from "@angular/core";
import { ListTeam } from "src/models/team";
import { BasicTableComponent } from "../basic-table.component";

@Component({
	selector: "team-table",
	templateUrl: "./team-table.component.html",
	styleUrls: ["../table.component.scss"],
})
export class TeamTableComponent extends BasicTableComponent<ListTeam> {
	public override innerColumns: string[] = ["id", "name", "createdAt"];

	public override compareData(
		a: ListTeam,
		b: ListTeam,
		isAsc: boolean,
		active: string
	): number {
		return this.compare((a as any)[active], (b as any)[active], isAsc);
	}
}
