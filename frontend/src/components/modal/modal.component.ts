import { Component, Input } from "@angular/core";

@Component({
	selector: "app-modal",
	templateUrl: "./modal.component.html",
	styleUrls: ["./modal.component.scss"],
})
export class ModalComponent {
	@Input() public open: boolean = false;
	@Input() public header?: string;
}
