import { Component, HostListener, OnDestroy, OnInit } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { Observable, Subject } from "rxjs";
import { takeUntil } from "rxjs/operators";
import { Deactivatable } from "src/models/deactivatable";
import { ErrorMessage } from "src/models/errors";
import { NotificationsService } from "src/services/notifications.service";

@Component({ template: "" })
export abstract class BasicOneComponent<T>
	implements OnInit, OnDestroy, Deactivatable
{
	public id: number = 0;
	public data?: T;
	public loading: boolean = false;
	public updating: boolean = false;
	public deleting: boolean = false;
	public abstract allRoute: string;

	protected readonly unsubscribe$: Subject<void> = new Subject<void>();

	public constructor(
		protected readonly route: ActivatedRoute,
		protected readonly router: Router,
		protected readonly snackbar: NotificationsService
	) {}

	@HostListener("window:beforeunload", ["$event"])
	public onBeforeUnload($event: Event): void {
		if (!this.canDeactivate()) {
			$event.returnValue = true;
		}
	}

	public ngOnInit(): void {
		this.route.params
			.pipe(takeUntil(this.unsubscribe$))
			.subscribe((params) => {
				this.id = params.id;
				this.fetch();
			});
	}

	public ngOnDestroy(): void {
		this.unsubscribe$.next();
		this.unsubscribe$.complete();
	}

	public openUpdateModal(): void {
		this.updating = true;
	}

	public openDeleteModal(): void {
		this.deleting = true;
	}

	public cancelUpdate(): void {
		this.updating = false;
	}

	public cancelDelete(): void {
		this.deleting = false;
	}

	public replace(data: T): void {
		this.cancelUpdate();
		this.data = data;
	}

	public remove(): void {
		this.cancelDelete();
		this.deleting = false;
		this.router.navigate([this.allRoute]);
	}

	public canDeactivate(): boolean {
		return !(this.updating || this.deleting);
	}

	public fetch(): void {
		this.loading = true;

		this.get()
			.pipe(takeUntil(this.unsubscribe$))
			.subscribe(
				(data) => {
					this.data = data;
					this.loading = false;
				},
				(error) => {
					if (
						error.hasOwnProperty("status") &&
						error.status === 404
					) {
						this.snackbar.error(ErrorMessage.NotFound);
					} else {
						this.snackbar.error(ErrorMessage.FailedToLoad);
					}

					this.router.navigate([this.allRoute]);
				}
			);
	}

	public abstract get(): Observable<T>;
}
