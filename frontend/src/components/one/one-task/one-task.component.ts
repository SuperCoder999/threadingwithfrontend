import { Component } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { Observable } from "rxjs";
import { TaskModel } from "src/models/task";
import { NotificationsService } from "src/services/notifications.service";
import { TaskService } from "src/services/task.service";
import { BasicOneComponent } from "../basic-one.component";

@Component({
	selector: "one-task",
	templateUrl: "one-task.component.html",
	styleUrls: ["../one.component.scss"],
})
export class OneTaskComponent extends BasicOneComponent<TaskModel> {
	public override allRoute: string = "/tasks";

	public constructor(
		private readonly service: TaskService,
		route: ActivatedRoute,
		router: Router,
		snackbar: NotificationsService
	) {
		super(route, router, snackbar);
	}

	public get(): Observable<TaskModel> {
		return this.service.getOne(this.id);
	}
}
