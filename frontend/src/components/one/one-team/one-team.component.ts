import { Component } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { Observable } from "rxjs";
import { Team } from "src/models/team";
import { NotificationsService } from "src/services/notifications.service";
import { TeamService } from "src/services/team.service";
import { BasicOneComponent } from "../basic-one.component";

@Component({
	selector: "one-team",
	templateUrl: "one-team.component.html",
	styleUrls: ["../one.component.scss"],
})
export class OneTeamComponent extends BasicOneComponent<Team> {
	public override allRoute: string = "/teams";

	public constructor(
		private readonly service: TeamService,
		route: ActivatedRoute,
		router: Router,
		snackbar: NotificationsService
	) {
		super(route, router, snackbar);
	}

	public get(): Observable<Team> {
		return this.service.getOne(this.id);
	}
}
