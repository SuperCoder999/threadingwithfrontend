import { Component } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { Observable } from "rxjs";
import { User } from "src/models/user";
import { NotificationsService } from "src/services/notifications.service";
import { UserService } from "src/services/user.service";
import { BasicOneComponent } from "../basic-one.component";

@Component({
	selector: "one-user",
	templateUrl: "one-user.component.html",
	styleUrls: ["../one.component.scss"],
})
export class OneUserComponent extends BasicOneComponent<User> {
	public override allRoute: string = "/users";

	public constructor(
		private readonly service: UserService,
		route: ActivatedRoute,
		router: Router,
		snackbar: NotificationsService
	) {
		super(route, router, snackbar);
	}

	public get(): Observable<User> {
		return this.service.getOne(this.id);
	}
}
