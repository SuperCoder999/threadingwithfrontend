import { Component } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { Observable } from "rxjs";
import { Project } from "src/models/project";
import { NotificationsService } from "src/services/notifications.service";
import { ProjectService } from "src/services/project.service";
import { BasicOneComponent } from "../basic-one.component";

@Component({
	selector: "one-project",
	templateUrl: "one-project.component.html",
	styleUrls: ["../one.component.scss"],
})
export class OneProjectComponent extends BasicOneComponent<Project> {
	public override allRoute: string = "/projects";

	public constructor(
		private readonly service: ProjectService,
		route: ActivatedRoute,
		router: Router,
		snackbar: NotificationsService
	) {
		super(route, router, snackbar);
	}

	public get(): Observable<Project> {
		return this.service.getOne(this.id);
	}
}
