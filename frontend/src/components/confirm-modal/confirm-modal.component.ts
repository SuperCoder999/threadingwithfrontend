import { Component, EventEmitter, Input, Output } from "@angular/core";

@Component({
	selector: "confirm-modal",
	templateUrl: "./confirm-modal.component.html",
	styleUrls: ["./confirm-modal.component.scss"],
})
export class ConfirmModalComponent {
	@Input() public message: string = "Are you sure?";
	@Input() public open: boolean = false;
	@Output() public onCancel: EventEmitter<void> = new EventEmitter<void>();
	@Output() public onConfirm: EventEmitter<void> = new EventEmitter<void>();

	public emitCancel(): void {
		this.onCancel.emit();
	}

	public emitConfirm(): void {
		this.onConfirm.emit();
	}
}
