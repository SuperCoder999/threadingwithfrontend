import {
	Component,
	Input,
	OnChanges,
	OnDestroy,
	OnInit,
	SimpleChanges,
} from "@angular/core";

import { FormControl, FormGroup, Validators } from "@angular/forms";
import { combineLatest, Observable, Subject } from "rxjs";
import { takeUntil } from "rxjs/operators";
import { nextDay } from "src/helpers/date.helper";
import { ErrorMessage } from "src/models/errors";
import { ModifyProject, ListProject, Project } from "src/models/project";
import { TeamOption } from "src/models/team";
import { UserOption } from "src/models/user";
import { NotificationsService } from "src/services/notifications.service";
import { TeamService } from "src/services/team.service";
import { UserService } from "src/services/user.service";
import { optionalMinLengthValidator } from "src/validators/optional-min-length.validator";
import { BasicFormComponent } from "../basic-form.component";

@Component({
	selector: "project-common-form",
	templateUrl: "./project-common-form.component.html",
	styleUrls: ["../form.component.scss"],
})
export class ProjectCommonFormComponent
	extends BasicFormComponent<ModifyProject>
	implements OnChanges, OnInit, OnDestroy
{
	@Input() public default?: ListProject;

	public minDeadline = nextDay();
	public userOptions: UserOption[] = [];
	public teamOptions: TeamOption[] = [];
	public loading: boolean = true;

	public override model: ModifyProject = {
		name: "",
		deadline: this.minDeadline,
	};

	public override schema: FormGroup = new FormGroup({
		name: new FormControl(this.model.name, [
			Validators.required,
			Validators.minLength(5),
			Validators.maxLength(200),
		]),
		description: new FormControl(this.model.description, [
			optionalMinLengthValidator(2),
			Validators.maxLength(2000),
		]),
		deadline: new FormControl(this.model.deadline, [Validators.required]),
		authorId: new FormControl(this.model.authorId),
		teamId: new FormControl(this.model.teamId),
	});

	public override validationErrorTexts = {
		name: {
			required: "Required",
			minlength: "Min length is 5 characters",
			maxlength: "Max length is 200 characters",
		},
		description: {
			minlength: "Min length is 2 characters",
			maxlength: "Max length is 2000 characters",
		},
		deadline: {
			required: "Required",
		},
	};

	private readonly unsubscribe$: Subject<void> = new Subject<void>();

	public constructor(
		private readonly teamService: TeamService,
		private readonly userService: UserService,
		private readonly snackbar: NotificationsService
	) {
		super();
	}

	public ngOnChanges(changes: SimpleChanges): void {
		if (changes["default"] && changes["default"].currentValue) {
			const mappedDefault = this.mapDefault(
				changes["default"].currentValue
			);

			if (mappedDefault) {
				this.model = mappedDefault;
			}

			this.assignFromModel();
		}
	}

	public ngOnInit(): void {
		const mappedDefault = this.mapDefault(this.default);

		if (mappedDefault) {
			this.model = mappedDefault;
		}

		this.loadData();
	}

	public ngOnDestroy(): void {
		this.unsubscribe$.next();
		this.unsubscribe$.complete();
	}

	private loadTeams$(): Observable<TeamOption[]> {
		return this.teamService.getOptions();
	}

	private loadUsers$(): Observable<UserOption[]> {
		return this.userService.getOptions();
	}

	private loadData(): void {
		const teams$ = this.loadTeams$();
		const users$ = this.loadUsers$();

		combineLatest([teams$, users$])
			.pipe(takeUntil(this.unsubscribe$))
			.subscribe(
				([teams, users]) => {
					this.loading = false;
					this.teamOptions = teams;
					this.userOptions = users;
				},
				() => {
					this.snackbar.error(ErrorMessage.FailedToLoad);
					this.loading = false;
					this.onCancel.emit();
				}
			);
	}

	private mapDefault(
		data: ListProject | undefined
	): ModifyProject | undefined {
		if (!data) {
			return;
		}

		return {
			...data,
			teamId: data.team?.id,
			authorId: data.author?.id,
		};
	}
}
