import {
	Component,
	Input,
	OnChanges,
	OnDestroy,
	OnInit,
	SimpleChanges,
} from "@angular/core";

import { FormControl, FormGroup, Validators } from "@angular/forms";
import { combineLatest, Observable, Subject } from "rxjs";
import { takeUntil } from "rxjs/operators";
import { lastDay } from "src/helpers/date.helper";
import { ProjectOption } from "src/models/project";
import { TaskModel, ModifyTask, TaskKanbanState } from "../../../models/task";
import { UserOption } from "src/models/user";
import { ProjectService } from "src/services/project.service";
import { UserService } from "src/services/user.service";
import { optionalMinLengthValidator } from "src/validators/optional-min-length.validator";
import { BasicFormComponent } from "../basic-form.component";
import { NotificationsService } from "src/services/notifications.service";
import { ErrorMessage } from "src/models/errors";

@Component({
	selector: "task-common-form",
	templateUrl: "./task-common-form.component.html",
	styleUrls: ["../form.component.scss"],
})
export class TaskCommonFormComponent
	extends BasicFormComponent<ModifyTask>
	implements OnChanges, OnInit, OnDestroy
{
	@Input() public default?: TaskModel;

	public loading: boolean = true;
	public projectOptions: ProjectOption[] = [];
	public userOptions: UserOption[] = [];
	public maxFinishedAt = lastDay();

	public override model: ModifyTask = {
		projectId: 0,
		name: "",
		state: TaskKanbanState.ToDo,
	};

	public override schema: FormGroup = new FormGroup({
		projectId: new FormControl(this.model.projectId),
		performerId: new FormControl(this.model.performerId),
		name: new FormControl(this.model.name, [
			Validators.required,
			Validators.minLength(5),
			Validators.maxLength(200),
		]),
		description: new FormControl(this.model.description, [
			optionalMinLengthValidator(5),
			Validators.maxLength(700),
		]),
		state: new FormControl(this.model.state),
		finishedAt: new FormControl(this.model.finishedAt),
	});

	public override validationErrorTexts = {
		name: {
			required: "Required",
			minlength: "Min length is 5 characters",
			maxlength: "Max length is 200 characters",
		},
		description: {
			minlength: "Min length is 2 characters",
			maxlength: "Max length is 700 characters",
		},
	};

	private readonly unsubscribe$: Subject<void> = new Subject<void>();

	public constructor(
		private readonly projectService: ProjectService,
		private readonly userService: UserService,
		private readonly snackbar: NotificationsService
	) {
		super();
	}

	public ngOnChanges(changes: SimpleChanges): void {
		if (changes["default"] && changes["default"].currentValue) {
			const mappedDefault = this.mapDefault(
				changes["default"].currentValue
			);

			if (mappedDefault) {
				this.model = mappedDefault;
			}

			this.assignFromModel();
		}
	}

	public ngOnInit(): void {
		const mappedDefault = this.mapDefault(this.default);

		if (mappedDefault) {
			this.model = mappedDefault;
		}

		this.loadData();
	}

	public ngOnDestroy(): void {
		this.unsubscribe$.next();
		this.unsubscribe$.complete();
	}

	private loadProjects$(): Observable<ProjectOption[]> {
		return this.projectService.getOptions();
	}

	private loadUsers$(): Observable<UserOption[]> {
		return this.userService.getOptions();
	}

	private loadData(): void {
		const users$ = this.loadUsers$();
		const projects$ = this.loadProjects$();

		combineLatest([users$, projects$])
			.pipe(takeUntil(this.unsubscribe$))
			.subscribe(
				([users, projects]) => {
					this.loading = false;
					this.userOptions = users;
					this.projectOptions = projects;
				},
				() => {
					this.snackbar.error(ErrorMessage.FailedToLoad);
					this.loading = false;
					this.onCancel.emit();
				}
			);
	}

	private mapDefault(data: TaskModel | undefined): ModifyTask | undefined {
		if (!data) {
			return;
		}

		return {
			...data,
			performerId: data.performer?.id,
		};
	}

	public log = console.log;
}
