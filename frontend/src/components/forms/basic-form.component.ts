import { Component, EventEmitter, Output } from "@angular/core";
import { FormGroup } from "@angular/forms";
import { removeOptionalStrings } from "src/helpers/optional-strings.helper";

@Component({ template: "" })
export abstract class BasicFormComponent<T> {
	@Output() public onSubmit: EventEmitter<T> = new EventEmitter<T>();
	@Output() public onCancel: EventEmitter<void> = new EventEmitter<void>();

	public abstract schema: FormGroup;
	public abstract model: T;

	public abstract validationErrorTexts: Record<
		string,
		Record<string, string>
	>;

	public submit(): void {
		if (this.isStrictValid()) {
			this.assign();
			this.onSubmit.emit(removeOptionalStrings<T>(this.model));

			this.schema.reset();
			this.assign();
		}
	}

	public cancel(): void {
		this.schema.reset();
		this.assign();

		this.onCancel.emit();
	}

	public getErrors(controlName: string): string[] {
		const control = this.schema.get(controlName);
		const isUntouched = !control?.dirty && !control?.touched;

		const noErrors =
			!control?.errors || Object.keys(control.errors).length === 0;

		if (isUntouched || noErrors) {
			return [];
		}

		return Object.keys(control!.errors!).map(
			(errorKey) => this.validationErrorTexts[controlName][errorKey]
		);
	}

	public assign(): void {
		this.model = this.schema.value;
	}

	public assignFromModel(): void {
		Object.entries(this.model).forEach(([key, value]) => {
			const control = this.schema.get(key);
			control?.setValue(value);
		});
	}

	public isStrictValid(): boolean {
		for (const [_, value] of Object.entries(this.schema.controls)) {
			if (value.invalid) {
				return false;
			}
		}

		return true;
	}
}
