import {
	Component,
	Input,
	OnChanges,
	OnInit,
	SimpleChanges,
} from "@angular/core";

import { FormControl, FormGroup, Validators } from "@angular/forms";
import { ModifyTeam, ListTeam } from "src/models/team";
import { BasicFormComponent } from "../basic-form.component";

@Component({
	selector: "team-common-form",
	templateUrl: "./team-common-form.component.html",
	styleUrls: ["../form.component.scss"],
})
export class TeamCommonFormComponent
	extends BasicFormComponent<ModifyTeam>
	implements OnChanges, OnInit
{
	@Input() public default?: ListTeam;

	public override model: ModifyTeam = {
		name: "",
	};

	public override schema: FormGroup = new FormGroup({
		name: new FormControl(this.model.name, [
			Validators.required,
			Validators.minLength(5),
			Validators.maxLength(200),
		]),
	});

	public override validationErrorTexts = {
		name: {
			required: "Required",
			minlength: "Min length is 5 characters",
			maxlength: "Max length is 200 characters",
		},
	};

	public ngOnChanges(changes: SimpleChanges): void {
		if (changes["default"] && changes["default"].currentValue) {
			this.model = changes["default"].currentValue;
			this.assignFromModel();
		}
	}

	public ngOnInit(): void {
		if (this.default) {
			this.model = this.default;
		}
	}
}
