import {
	Component,
	Input,
	OnChanges,
	OnDestroy,
	OnInit,
	SimpleChanges,
} from "@angular/core";

import { FormGroup, FormControl, Validators } from "@angular/forms";
import { Subject } from "rxjs";
import { takeUntil } from "rxjs/operators";
import { lastDay } from "src/helpers/date.helper";
import { ErrorMessage } from "src/models/errors";
import { TeamOption } from "src/models/team";
import { ModifyUser, User } from "src/models/user";
import { NotificationsService } from "src/services/notifications.service";
import { TeamService } from "src/services/team.service";
import { strictEmailValidator } from "src/validators/email.validator";
import { optionalMinLengthValidator } from "src/validators/optional-min-length.validator";
import { BasicFormComponent } from "../basic-form.component";

@Component({
	selector: "user-common-form",
	templateUrl: "./user-common-form.component.html",
	styleUrls: ["../form.component.scss"],
})
export class UserCommonFormComponent
	extends BasicFormComponent<ModifyUser>
	implements OnChanges, OnInit, OnDestroy
{
	@Input() public default?: User;

	public loading: boolean = true;
	public teamOptions: TeamOption[] = [];

	public maxBirthDay: Date = lastDay();

	public override model: ModifyUser = {
		teamId: 0,
		email: "",
		birthDay: this.maxBirthDay,
	};

	public override schema: FormGroup = new FormGroup({
		teamId: new FormControl(this.model.teamId),
		email: new FormControl(this.model.email, [
			Validators.required,
			strictEmailValidator,
		]),
		firstName: new FormControl(this.model.firstName, [
			optionalMinLengthValidator(2),
			Validators.maxLength(50),
		]),
		lastName: new FormControl(this.model.lastName, [
			optionalMinLengthValidator(2),
			Validators.maxLength(50),
		]),
		birthDay: new FormControl(this.model.birthDay, [Validators.required]),
	});

	public override validationErrorTexts = {
		email: {
			required: "Required",
			email: "Must be an email address",
		},
		firstName: {
			minlength: "Min length is 2 characters",
			maxlength: "Max length is 50 characters",
		},
		lastName: {
			minlength: "Min length is 2 characters",
			maxlength: "Max length is 50 characters",
		},
		birthDay: {
			required: "Required",
		},
	};

	private readonly unsubscribe$: Subject<void> = new Subject<void>();

	public constructor(
		private readonly teamService: TeamService,
		private readonly snackbar: NotificationsService
	) {
		super();
	}

	public ngOnChanges(changes: SimpleChanges): void {
		if (changes["default"] && changes["default"].currentValue) {
			this.model = changes["default"].currentValue;
			this.assignFromModel();
		}
	}

	public ngOnInit(): void {
		if (this.default) {
			this.model = this.default;
		}

		this.loadTeams();
	}

	public ngOnDestroy(): void {
		this.unsubscribe$.next();
		this.unsubscribe$.complete();
	}

	private loadTeams(): void {
		this.teamService
			.getOptions()
			.pipe(takeUntil(this.unsubscribe$))
			.subscribe(
				(teams) => {
					this.loading = false;
					this.teamOptions = teams;
				},
				() => {
					this.snackbar.error(ErrorMessage.FailedToLoad);
					this.loading = false;
					this.onCancel.emit();
				}
			);
	}
}
