import { Component } from "@angular/core";
import { Router } from "@angular/router";

@Component({
	selector: "landing-page",
	templateUrl: "./landing-page.component.html",
	styleUrls: ["./landing-page.component.scss"],
})
export class LandingPageComponent {
	public constructor(private readonly router: Router) {}

	public users(): void {
		this.router.navigate(["/users"]);
	}

	public teams(): void {
		this.router.navigate(["/teams"]);
	}

	public tasks(): void {
		this.router.navigate(["/tasks"]);
	}

	public projects(): void {
		this.router.navigate(["/projects"]);
	}
}
