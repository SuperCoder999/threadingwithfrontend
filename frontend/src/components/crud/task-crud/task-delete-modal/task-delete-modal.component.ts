import { Component } from "@angular/core";
import { Observable } from "rxjs";
import { NotificationsService } from "src/services/notifications.service";
import { TaskService } from "src/services/task.service";
import { BasicDeleteModalComponent } from "../../basic-delete-modal.component";

@Component({
	selector: "task-delete-modal",
	templateUrl: "../../delete-modal.component.html",
})
export class TaskDeleteModalComponent extends BasicDeleteModalComponent {
	public constructor(
		private readonly service: TaskService,
		snackbar: NotificationsService
	) {
		super(snackbar);
	}

	public override sendDelete(id: number): Observable<void> {
		return this.service.delete(id);
	}
}
