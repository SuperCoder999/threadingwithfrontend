import { Component } from "@angular/core";
import { Observable } from "rxjs";
import { TaskModel, ModifyTask } from "src/models/task";
import { NotificationsService } from "src/services/notifications.service";
import { TaskService } from "src/services/task.service";
import { BasicUpdateModalComponent } from "../../basic-update-modal.component";

@Component({
	selector: "task-update-modal",
	templateUrl: "./task-update-modal.component.html",
})
export class TaskUpdateModalComponent extends BasicUpdateModalComponent<
	TaskModel,
	TaskModel,
	ModifyTask
> {
	public constructor(
		private readonly service: TaskService,
		snackbar: NotificationsService
	) {
		super(snackbar);
	}

	public override sendUpdate(
		id: number,
		data: ModifyTask
	): Observable<TaskModel> {
		return this.service.update(id, data);
	}
}
