import { Component } from "@angular/core";
import { Observable } from "rxjs";
import { TaskService } from "src/services/task.service";
import { TaskModel } from "src/models/task";
import { BasicCrudComponent } from "../basic-crud.component";
import { NotificationsService } from "src/services/notifications.service";

@Component({
	selector: "task-crud",
	templateUrl: "./task-crud.component.html",
	styleUrls: ["../crud.component.scss"],
})
export class TaskCrudComponent extends BasicCrudComponent<TaskModel> {
	public constructor(
		private readonly service: TaskService,
		snackbar: NotificationsService
	) {
		super(snackbar);
	}

	public override get(): Observable<TaskModel[]> {
		return this.service.get();
	}
}
