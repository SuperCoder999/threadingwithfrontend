import { Component } from "@angular/core";
import { Observable } from "rxjs";
import { TaskModel, ModifyTask } from "src/models/task";
import { NotificationsService } from "src/services/notifications.service";
import { TaskService } from "src/services/task.service";
import { BasicCreateModalComponent } from "../../basic-create-modal.component";

@Component({
	selector: "task-create-modal",
	templateUrl: "./task-create-modal.component.html",
})
export class TaskCreateModalComponent extends BasicCreateModalComponent<
	TaskModel,
	ModifyTask
> {
	public constructor(
		private readonly service: TaskService,
		snackbar: NotificationsService
	) {
		super(snackbar);
	}

	public override sendCreate(data: ModifyTask): Observable<TaskModel> {
		return this.service.create(data);
	}
}
