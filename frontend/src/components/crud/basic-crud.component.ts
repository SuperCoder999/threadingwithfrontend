import { Component, OnInit, OnDestroy, HostListener } from "@angular/core";
import { Observable, Subject } from "rxjs";
import { takeUntil } from "rxjs/operators";
import { Deactivatable } from "src/models/deactivatable";
import { ErrorMessage } from "src/models/errors";
import { NotificationsService } from "src/services/notifications.service";

@Component({ template: "" })
export abstract class BasicCrudComponent<T extends { id: number }>
	implements OnInit, OnDestroy, Deactivatable
{
	public data: T[] = [];
	public loading: boolean = true;
	public creating: boolean = false;
	public updating: boolean = false;
	public updatingObject?: T;
	public deleting: boolean = false;
	public deletingId?: number;

	protected readonly unsubscribe$: Subject<void> = new Subject<void>();

	public constructor(protected readonly snackbar: NotificationsService) {}

	@HostListener("window:beforeunload", ["$event"])
	public onBeforeUnload($event: Event): void {
		if (!this.canDeactivate()) {
			$event.returnValue = true;
		}
	}

	public ngOnInit(): void {
		this.fetch();
	}

	public ngOnDestroy(): void {
		this.unsubscribe$.next();
		this.unsubscribe$.complete();
	}

	public fetch(): void {
		this.get()
			.pipe(takeUntil(this.unsubscribe$))
			.subscribe(
				(data) => {
					this.data = data;
					this.loading = false;
				},
				() => {
					this.snackbar.error(ErrorMessage.FailedToLoad);
					this.loading = false;
				}
			);
	}

	public openCreateModal(): void {
		this.creating = true;
	}

	public cancelCreate(): void {
		this.creating = false;
	}

	public add(item: T): void {
		this.cancelCreate();
		this.data = [...this.data, item];
	}

	public openUpdateModal(item: T): void {
		this.updating = true;
		this.updatingObject = item;
	}

	public cancelUpdate(): void {
		this.updating = false;
		this.updatingObject = undefined;
	}

	public replace(item: T): void {
		this.cancelUpdate();
		const newData = [...this.data];

		const index = newData.findIndex((suspect) => suspect.id === item.id);

		if (index < 0) {
			return;
		}

		newData[index] = item;
		this.data = [...newData];
	}

	public openDeleteModal(item: T): void {
		this.deleting = true;
		this.deletingId = item.id;
	}

	public cancelDelete(): void {
		this.deleting = false;
		this.deletingId = undefined;
	}

	public remove(id: number): void {
		this.cancelDelete();

		const newData = [...this.data];
		const index = newData.findIndex((item) => item.id === id);

		if (index < 0) {
			return;
		}

		newData.splice(index, 1);
		this.data = [...newData];
	}

	public canDeactivate(): boolean {
		return !(this.creating || this.updating || this.deleting);
	}

	public abstract get(): Observable<T[]>;
}
