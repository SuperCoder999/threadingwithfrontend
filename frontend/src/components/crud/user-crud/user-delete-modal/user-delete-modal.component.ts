import { Component } from "@angular/core";
import { Observable } from "rxjs";
import { NotificationsService } from "src/services/notifications.service";
import { UserService } from "src/services/user.service";
import { BasicDeleteModalComponent } from "../../basic-delete-modal.component";

@Component({
	selector: "user-delete-modal",
	templateUrl: "../../delete-modal.component.html",
})
export class UserDeleteModalComponent extends BasicDeleteModalComponent {
	public constructor(
		private readonly service: UserService,
		snackbar: NotificationsService
	) {
		super(snackbar);
	}

	public override sendDelete(id: number): Observable<void> {
		return this.service.delete(id);
	}
}
