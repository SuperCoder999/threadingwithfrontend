import { Component } from "@angular/core";
import { Observable } from "rxjs";
import { User } from "src/models/user";
import { NotificationsService } from "src/services/notifications.service";
import { UserService } from "src/services/user.service";
import { BasicCrudComponent } from "../basic-crud.component";

@Component({
	selector: "user-crud",
	templateUrl: "./user-crud.component.html",
	styleUrls: ["../crud.component.scss"],
})
export class UserCrudComponent extends BasicCrudComponent<User> {
	public constructor(
		private readonly service: UserService,
		snackbar: NotificationsService
	) {
		super(snackbar);
	}

	public override get(): Observable<User[]> {
		return this.service.get();
	}
}
