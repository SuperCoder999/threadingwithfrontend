import { Component } from "@angular/core";
import { Observable } from "rxjs";
import { User, ModifyUser } from "src/models/user";
import { NotificationsService } from "src/services/notifications.service";
import { UserService } from "src/services/user.service";
import { BasicUpdateModalComponent } from "../../basic-update-modal.component";

@Component({
	selector: "user-update-modal",
	templateUrl: "./user-update-modal.component.html",
})
export class UserUpdateModalComponent extends BasicUpdateModalComponent<
	User,
	User,
	ModifyUser
> {
	public constructor(
		private readonly service: UserService,
		snackbar: NotificationsService
	) {
		super(snackbar);
	}

	public override sendUpdate(id: number, data: ModifyUser): Observable<User> {
		return this.service.update(id, data);
	}
}
