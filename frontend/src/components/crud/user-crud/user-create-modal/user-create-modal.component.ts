import { Component } from "@angular/core";
import { Observable } from "rxjs";
import { ModifyUser, User } from "src/models/user";
import { NotificationsService } from "src/services/notifications.service";
import { UserService } from "src/services/user.service";
import { BasicCreateModalComponent } from "../../basic-create-modal.component";

@Component({
	selector: "user-create-modal",
	templateUrl: "./user-create-modal.component.html",
})
export class UserCreateModalComponent extends BasicCreateModalComponent<
	User,
	ModifyUser
> {
	public constructor(
		private readonly service: UserService,
		snackbar: NotificationsService
	) {
		super(snackbar);
	}

	public override sendCreate(data: ModifyUser): Observable<User> {
		return this.service.create(data);
	}
}
