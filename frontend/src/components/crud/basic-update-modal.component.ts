import {
	Component,
	EventEmitter,
	Input,
	OnDestroy,
	Output,
} from "@angular/core";
import { Observable, Subject } from "rxjs";
import { takeUntil } from "rxjs/operators";
import { ErrorMessage } from "src/models/errors";
import { NotificationsService } from "src/services/notifications.service";

@Component({ template: "" })
export abstract class BasicUpdateModalComponent<T, TExisting, TUpdate>
	implements OnDestroy
{
	@Input() public open: boolean = false;
	@Input() public id?: number;
	@Input() public existing?: TExisting;
	@Output() public onCancel: EventEmitter<void> = new EventEmitter<void>();
	@Output() public onUpdate: EventEmitter<T> = new EventEmitter<T>();

	public loading: boolean = false;

	protected readonly unsubscribe$: Subject<void> = new Subject<void>();

	public constructor(protected readonly snackbar: NotificationsService) {}

	public ngOnDestroy(): void {
		this.unsubscribe$.next();
		this.unsubscribe$.complete();
	}

	public update(data: TUpdate): void {
		if (this.id === undefined) {
			return;
		}

		this.loading = true;

		this.sendUpdate(this.id, data)
			.pipe(takeUntil(this.unsubscribe$))
			.subscribe(
				(updated) => {
					this.loading = false;
					this.onUpdate.emit(updated);
				},
				() => {
					this.snackbar.error(ErrorMessage.FailedToUpdate);
					this.loading = false;
					this.onCancel.emit();
				}
			);
	}

	public cancel(): void {
		this.onCancel.emit();
	}

	public abstract sendUpdate(id: number, data: TUpdate): Observable<T>;
}
