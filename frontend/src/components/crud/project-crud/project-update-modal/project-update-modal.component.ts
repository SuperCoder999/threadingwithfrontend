import { Component } from "@angular/core";
import { Observable } from "rxjs";
import { ListProject, ModifyProject, Project } from "src/models/project";
import { NotificationsService } from "src/services/notifications.service";
import { ProjectService } from "src/services/project.service";
import { BasicUpdateModalComponent } from "../../basic-update-modal.component";

@Component({
	selector: "project-update-modal",
	templateUrl: "./project-update-modal.component.html",
})
export class ProjectUpdateModalComponent extends BasicUpdateModalComponent<
	Project,
	ListProject,
	ModifyProject
> {
	public constructor(
		private readonly service: ProjectService,
		snackbar: NotificationsService
	) {
		super(snackbar);
	}

	public override sendUpdate(
		id: number,
		data: ModifyProject
	): Observable<Project> {
		return this.service.update(id, data);
	}
}
