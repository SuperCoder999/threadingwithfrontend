import { Component } from "@angular/core";
import { Observable } from "rxjs";
import { NotificationsService } from "src/services/notifications.service";
import { ProjectService } from "src/services/project.service";
import { BasicDeleteModalComponent } from "../../basic-delete-modal.component";

@Component({
	selector: "project-delete-modal",
	templateUrl: "../../delete-modal.component.html",
})
export class ProjectDeleteModalComponent extends BasicDeleteModalComponent {
	public constructor(
		private readonly service: ProjectService,
		snackbar: NotificationsService
	) {
		super(snackbar);
	}

	public override sendDelete(id: number): Observable<void> {
		return this.service.delete(id);
	}
}
