import { Component } from "@angular/core";
import { Observable } from "rxjs";
import { ModifyProject, Project } from "src/models/project";
import { NotificationsService } from "src/services/notifications.service";
import { ProjectService } from "src/services/project.service";
import { BasicCreateModalComponent } from "../../basic-create-modal.component";

@Component({
	selector: "project-create-modal",
	templateUrl: "./project-create-modal.component.html",
})
export class ProjectCreateModalComponent extends BasicCreateModalComponent<
	Project,
	ModifyProject
> {
	public constructor(
		private readonly service: ProjectService,
		snackbar: NotificationsService
	) {
		super(snackbar);
	}

	public override sendCreate(data: ModifyProject): Observable<Project> {
		return this.service.create(data);
	}
}
