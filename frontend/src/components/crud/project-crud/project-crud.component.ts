import { Component } from "@angular/core";
import { Observable } from "rxjs";
import { ListProject } from "src/models/project";
import { NotificationsService } from "src/services/notifications.service";
import { ProjectService } from "src/services/project.service";
import { BasicCrudComponent } from "../basic-crud.component";

@Component({
	selector: "project-crud",
	templateUrl: "./project-crud.component.html",
	styleUrls: ["../crud.component.scss"],
})
export class ProjectCrudComponent extends BasicCrudComponent<ListProject> {
	public constructor(
		private readonly service: ProjectService,
		snackbar: NotificationsService
	) {
		super(snackbar);
	}

	public override get(): Observable<ListProject[]> {
		return this.service.get();
	}
}
