import { EventEmitter, Input, OnDestroy, Output } from "@angular/core";
import { Component } from "@angular/core";
import { Observable, Subject } from "rxjs";
import { takeUntil } from "rxjs/operators";
import { ErrorMessage } from "src/models/errors";
import { NotificationsService } from "src/services/notifications.service";

@Component({ template: "" })
export abstract class BasicCreateModalComponent<T, TCreate>
	implements OnDestroy
{
	@Input() public open: boolean = false;
	@Output() public onCancel: EventEmitter<void> = new EventEmitter<void>();
	@Output() public onCreate: EventEmitter<T> = new EventEmitter<T>();

	public loading: boolean = false;

	protected readonly unsubscribe$: Subject<void> = new Subject<void>();

	public constructor(protected readonly snackbar: NotificationsService) {}

	public ngOnDestroy(): void {
		this.unsubscribe$.next();
		this.unsubscribe$.complete();
	}

	public create(data: TCreate): void {
		this.loading = true;

		this.sendCreate(data)
			.pipe(takeUntil(this.unsubscribe$))
			.subscribe(
				(created) => {
					this.loading = false;
					this.onCreate.emit(created);
				},
				() => {
					this.snackbar.error(ErrorMessage.FailedToCreate);
					this.loading = false;
					this.onCancel.emit();
				}
			);
	}

	public cancel(): void {
		this.onCancel.emit();
	}

	public abstract sendCreate(data: TCreate): Observable<T>;
}
