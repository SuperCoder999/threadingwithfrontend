import { Component } from "@angular/core";
import { Observable } from "rxjs";
import { ListTeam, ModifyTeam, Team } from "src/models/team";
import { NotificationsService } from "src/services/notifications.service";
import { TeamService } from "src/services/team.service";
import { BasicUpdateModalComponent } from "../../basic-update-modal.component";

@Component({
	selector: "team-update-modal",
	templateUrl: "./team-update-modal.component.html",
})
export class TeamUpdateModalComponent extends BasicUpdateModalComponent<
	Team,
	ListTeam,
	ModifyTeam
> {
	public constructor(
		private readonly service: TeamService,
		snackbar: NotificationsService
	) {
		super(snackbar);
	}

	public sendUpdate(id: number, data: ModifyTeam): Observable<Team> {
		return this.service.update(id, data);
	}
}
