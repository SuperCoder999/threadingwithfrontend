import { Component } from "@angular/core";
import { Observable } from "rxjs";
import { ModifyTeam, Team } from "src/models/team";
import { NotificationsService } from "src/services/notifications.service";
import { TeamService } from "src/services/team.service";
import { BasicCreateModalComponent } from "../../basic-create-modal.component";

@Component({
	selector: "team-create-modal",
	templateUrl: "./team-create-modal.component.html",
})
export class TeamCreateModalComponent extends BasicCreateModalComponent<
	Team,
	ModifyTeam
> {
	public constructor(
		private readonly service: TeamService,
		snackbar: NotificationsService
	) {
		super(snackbar);
	}

	public override sendCreate(data: ModifyTeam): Observable<Team> {
		return this.service.create(data);
	}
}
