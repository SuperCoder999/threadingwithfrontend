import { Component } from "@angular/core";
import { Observable } from "rxjs";
import { ListTeam } from "src/models/team";
import { NotificationsService } from "src/services/notifications.service";
import { TeamService } from "src/services/team.service";
import { BasicCrudComponent } from "../basic-crud.component";

@Component({
	selector: "team-crud",
	templateUrl: "./team-crud.component.html",
	styleUrls: ["../crud.component.scss"],
})
export class TeamCrudComponent extends BasicCrudComponent<ListTeam> {
	public constructor(
		private readonly service: TeamService,
		snackbar: NotificationsService
	) {
		super(snackbar);
	}

	public override get(): Observable<ListTeam[]> {
		return this.service.get();
	}
}
