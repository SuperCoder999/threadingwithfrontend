import { Component } from "@angular/core";
import { Observable } from "rxjs";
import { NotificationsService } from "src/services/notifications.service";
import { TeamService } from "src/services/team.service";
import { BasicDeleteModalComponent } from "../../basic-delete-modal.component";

@Component({
	selector: "team-delete-modal",
	templateUrl: "../../delete-modal.component.html",
})
export class TeamDeleteModalComponent extends BasicDeleteModalComponent {
	public constructor(
		private readonly service: TeamService,
		snackbar: NotificationsService
	) {
		super(snackbar);
	}

	public override sendDelete(id: number): Observable<void> {
		return this.service.delete(id);
	}
}
