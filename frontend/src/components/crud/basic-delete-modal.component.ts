import {
	Component,
	EventEmitter,
	Input,
	OnDestroy,
	Output,
} from "@angular/core";

import { Observable, Subject } from "rxjs";
import { takeUntil } from "rxjs/operators";
import { ErrorMessage } from "src/models/errors";
import { NotificationsService } from "src/services/notifications.service";

@Component({ template: "" })
export abstract class BasicDeleteModalComponent implements OnDestroy {
	@Input() public id?: number;
	@Input() public open: boolean = false;
	@Output() public onCancel: EventEmitter<void> = new EventEmitter<void>();

	@Output() public onDelete: EventEmitter<number> =
		new EventEmitter<number>();

	public loading: boolean = false;

	protected readonly unsubscribe$: Subject<void> = new Subject<void>();

	public constructor(protected readonly snackbar: NotificationsService) {}

	public ngOnDestroy(): void {
		this.unsubscribe$.next();
		this.unsubscribe$.complete();
	}

	public emitDelete(): void {
		if (this.id === undefined) {
			return;
		}

		this.loading = true;

		this.sendDelete(this.id)
			.pipe(takeUntil(this.unsubscribe$))
			.subscribe(
				() => {
					this.loading = false;
					this.onDelete.emit(this.id);
				},
				() => {
					this.snackbar.error(ErrorMessage.FailedToDelete);
					this.loading = false;
					this.onCancel.emit();
				}
			);
	}

	public emitCancel(): void {
		this.onCancel.emit();
	}

	public abstract sendDelete(id: number): Observable<void>;
}
