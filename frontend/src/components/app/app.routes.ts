import { Routes } from "@angular/router";
import { DeactivationGuard } from "src/guard/deactivation.guard";
import { ProjectCrudComponent } from "../crud/project-crud/project-crud.component";
import { TaskCrudComponent } from "../crud/task-crud/task-crud.component";
import { TeamCrudComponent } from "../crud/team-crud/team-crud.component";
import { UserCrudComponent } from "../crud/user-crud/user-crud.component";
import { LandingPageComponent } from "../landing-page/landing-page.component";
import { OneProjectComponent } from "../one/one-project/one-project.component";
import { OneTaskComponent } from "../one/one-task/one-task.component";
import { OneTeamComponent } from "../one/one-team/one-team.component";
import { OneUserComponent } from "../one/one-user/one-user.component";

const routes: Routes = [
	{
		path: "teams",
		pathMatch: "full",
		component: TeamCrudComponent,
		canDeactivate: [DeactivationGuard],
	},
	{
		path: "users",
		pathMatch: "full",
		component: UserCrudComponent,
		canDeactivate: [DeactivationGuard],
	},
	{
		path: "tasks",
		pathMatch: "full",
		component: TaskCrudComponent,
		canDeactivate: [DeactivationGuard],
	},
	{
		path: "projects",
		pathMatch: "full",
		component: ProjectCrudComponent,
		canDeactivate: [DeactivationGuard],
	},
	{
		path: "team/:id",
		pathMatch: "full",
		component: OneTeamComponent,
		canDeactivate: [DeactivationGuard],
	},
	{
		path: "user/:id",
		pathMatch: "full",
		component: OneUserComponent,
		canDeactivate: [DeactivationGuard],
	},
	{
		path: "task/:id",
		pathMatch: "full",
		component: OneTaskComponent,
		canDeactivate: [DeactivationGuard],
	},
	{
		path: "project/:id",
		pathMatch: "full",
		component: OneProjectComponent,
		canDeactivate: [DeactivationGuard],
	},
	{
		path: "",
		pathMatch: "full",
		component: LandingPageComponent,
	},
	{
		path: "**",
		redirectTo: "",
	},
];

export default routes;
