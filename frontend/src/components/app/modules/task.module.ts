import { NgModule } from "@angular/core";
import { ReactiveFormsModule } from "@angular/forms";
import { BrowserModule } from "@angular/platform-browser";
import { TaskCreateModalComponent } from "src/components/crud/task-crud/task-create-modal/task-create-modal.component";
import { TaskCrudComponent } from "src/components/crud/task-crud/task-crud.component";
import { TaskDeleteModalComponent } from "src/components/crud/task-crud/task-delete-modal/task-delete-modal.component";
import { TaskUpdateModalComponent } from "src/components/crud/task-crud/task-update-modal/task-update-modal.component";
import { TaskCommonFormComponent } from "src/components/forms/task-common-form/task-common-form.component";
import { OneTaskComponent } from "src/components/one/one-task/one-task.component";
import { TaskTableComponent } from "src/components/table/task-table/task-table.component";
import { CommonComponentsModule } from "./common-components.module";
import { HelpersModule } from "./helpers.module";
import { MaterialModule } from "./material.module";
import { RoutingModule } from "./routing.module";

@NgModule({
	declarations: [
		TaskCrudComponent,
		TaskTableComponent,
		TaskCommonFormComponent,
		TaskCreateModalComponent,
		TaskUpdateModalComponent,
		TaskDeleteModalComponent,
		OneTaskComponent,
	],
	imports: [
		BrowserModule,
		ReactiveFormsModule,
		MaterialModule,
		CommonComponentsModule,
		HelpersModule,
		RoutingModule,
	],
	exports: [TaskTableComponent],
})
export class TaskModule {}
