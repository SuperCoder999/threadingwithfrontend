import { NgModule } from "@angular/core";
import { ReactiveFormsModule } from "@angular/forms";
import { BrowserModule } from "@angular/platform-browser";
import { UserCreateModalComponent } from "src/components/crud/user-crud/user-create-modal/user-create-modal.component";
import { UserCrudComponent } from "src/components/crud/user-crud/user-crud.component";
import { UserDeleteModalComponent } from "src/components/crud/user-crud/user-delete-modal/user-delete-modal.component";
import { UserUpdateModalComponent } from "src/components/crud/user-crud/user-update-modal/user-update-modal.component";
import { UserCommonFormComponent } from "src/components/forms/user-common-form/user-common-form.component";
import { OneUserComponent } from "src/components/one/one-user/one-user.component";
import { UserTableComponent } from "src/components/table/user-table/user-table.component";
import { CommonComponentsModule } from "./common-components.module";
import { HelpersModule } from "./helpers.module";
import { MaterialModule } from "./material.module";
import { RoutingModule } from "./routing.module";

@NgModule({
	declarations: [
		UserCrudComponent,
		UserTableComponent,
		UserCommonFormComponent,
		UserCreateModalComponent,
		UserUpdateModalComponent,
		UserDeleteModalComponent,
		OneUserComponent,
	],
	imports: [
		BrowserModule,
		ReactiveFormsModule,
		MaterialModule,
		CommonComponentsModule,
		HelpersModule,
		RoutingModule,
	],
	exports: [UserTableComponent],
})
export class UserModule {}
