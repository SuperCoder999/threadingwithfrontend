import { HttpClientModule, HTTP_INTERCEPTORS } from "@angular/common/http";
import { NgModule } from "@angular/core";
import { BrowserModule } from "@angular/platform-browser";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { RoutingModule } from "./routing.module";
import { BaseUrlInterceptor } from "src/interceptors/base-url.interceptor";
import { AppComponent } from "../app.component";
import { DeactivationGuard } from "src/guard/deactivation.guard";
import { UserModule } from "./user.module";
import { TeamModule } from "./team.module";
import { TaskModule } from "./task.module";
import { ProjectModule } from "./project.module";
import { LandingPageComponent } from "src/components/landing-page/landing-page.component";
import { MaterialModule } from "./material.module";

@NgModule({
	declarations: [AppComponent, LandingPageComponent],
	imports: [
		BrowserModule,
		RoutingModule,
		HttpClientModule,
		BrowserAnimationsModule,
		MaterialModule,
		UserModule,
		TeamModule,
		TaskModule,
		ProjectModule,
	],
	providers: [
		{
			provide: HTTP_INTERCEPTORS,
			useClass: BaseUrlInterceptor,
			multi: true,
		},
		DeactivationGuard,
	],
	bootstrap: [AppComponent],
})
export class AppModule {}
