import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { ConfirmModalComponent } from "src/components/confirm-modal/confirm-modal.component";
import { DimmerComponent } from "src/components/dimmer/dimmer.component";
import { ModalComponent } from "src/components/modal/modal.component";
import { SpinnerComponent } from "src/components/spinner/spinner.component";
import { MaterialModule } from "./material.module";

const components = [
	DimmerComponent,
	ModalComponent,
	SpinnerComponent,
	ConfirmModalComponent,
];

@NgModule({
	declarations: components,
	imports: [CommonModule, MaterialModule],
	exports: components,
})
export class CommonComponentsModule {}
