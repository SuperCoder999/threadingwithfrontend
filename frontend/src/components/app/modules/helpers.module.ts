import { NgModule } from "@angular/core";
import { TaskKanbanStateDirective } from "src/directives/task-state.directive";
import { DateFormatPipe } from "src/pipes/date.pipe";
import { TaskKanbanStatePipe } from "src/pipes/task-state.pipe";
import { UsernamePipe } from "src/pipes/username.pipe";

const helpers = [
	DateFormatPipe,
	TaskKanbanStatePipe,
	UsernamePipe,
	TaskKanbanStateDirective,
];

@NgModule({
	declarations: helpers,
	exports: helpers,
})
export class HelpersModule {}
