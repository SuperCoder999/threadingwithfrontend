import { NgModule } from "@angular/core";
import { ReactiveFormsModule } from "@angular/forms";
import { BrowserModule } from "@angular/platform-browser";
import { TeamCreateModalComponent } from "src/components/crud/team-crud/team-create-modal/team-create-modal.component";
import { TeamCrudComponent } from "src/components/crud/team-crud/team-crud.component";
import { TeamDeleteModalComponent } from "src/components/crud/team-crud/team-delete-modal/team-delete-modal.component";
import { TeamUpdateModalComponent } from "src/components/crud/team-crud/team-update-modal/team-update-modal.component";
import { TeamCommonFormComponent } from "src/components/forms/team-common-form/team-common-form.component";
import { OneTeamComponent } from "src/components/one/one-team/one-team.component";
import { TeamTableComponent } from "src/components/table/team-table/team-table.component";
import { CommonComponentsModule } from "./common-components.module";
import { HelpersModule } from "./helpers.module";
import { MaterialModule } from "./material.module";
import { RoutingModule } from "./routing.module";
import { UserModule } from "./user.module";

@NgModule({
	declarations: [
		TeamCrudComponent,
		TeamTableComponent,
		TeamCommonFormComponent,
		TeamCreateModalComponent,
		TeamUpdateModalComponent,
		TeamDeleteModalComponent,
		OneTeamComponent,
	],
	imports: [
		BrowserModule,
		ReactiveFormsModule,
		MaterialModule,
		CommonComponentsModule,
		HelpersModule,
		RoutingModule,
		UserModule,
	],
	exports: [TeamTableComponent],
})
export class TeamModule {}
