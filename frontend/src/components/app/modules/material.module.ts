import { NgModule } from "@angular/core";
import { MatNativeDateModule } from "@angular/material/core";
import { MatButtonModule } from "@angular/material/button";
import { MatIconModule } from "@angular/material/icon";
import { MatTableModule } from "@angular/material/table";
import { MatFormFieldModule } from "@angular/material/form-field";
import { MatInputModule } from "@angular/material/input";
import { MatSelectModule } from "@angular/material/select";
import { MatDatepickerModule } from "@angular/material/datepicker";
import { MatSortModule } from "@angular/material/sort";
import { MatSnackBarModule } from "@angular/material/snack-bar";

export const materialModules = [
	MatButtonModule,
	MatIconModule,
	MatTableModule,
	MatSortModule,
	MatFormFieldModule,
	MatInputModule,
	MatSelectModule,
	MatDatepickerModule,
	MatNativeDateModule,
	MatSnackBarModule,
];

@NgModule({
	imports: materialModules,
	exports: materialModules,
})
export class MaterialModule {}
