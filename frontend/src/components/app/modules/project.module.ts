import { NgModule } from "@angular/core";
import { ReactiveFormsModule } from "@angular/forms";
import { BrowserModule } from "@angular/platform-browser";
import { ProjectCreateModalComponent } from "src/components/crud/project-crud/project-create-modal/project-create-modal.component";
import { ProjectCrudComponent } from "src/components/crud/project-crud/project-crud.component";
import { ProjectDeleteModalComponent } from "src/components/crud/project-crud/project-delete-modal/project-delete-modal.component";
import { ProjectUpdateModalComponent } from "src/components/crud/project-crud/project-update-modal/project-update-modal.component";
import { ProjectCommonFormComponent } from "src/components/forms/project-common-form/project-common-form.component";
import { OneProjectComponent } from "src/components/one/one-project/one-project.component";
import { ProjectTableComponent } from "src/components/table/project-table/project-table.component";
import { CommonComponentsModule } from "./common-components.module";
import { HelpersModule } from "./helpers.module";
import { MaterialModule } from "./material.module";
import { RoutingModule } from "./routing.module";
import { TaskModule } from "./task.module";

@NgModule({
	declarations: [
		ProjectCrudComponent,
		ProjectTableComponent,
		ProjectCommonFormComponent,
		ProjectCreateModalComponent,
		ProjectUpdateModalComponent,
		ProjectDeleteModalComponent,
		OneProjectComponent,
	],
	imports: [
		BrowserModule,
		ReactiveFormsModule,
		MaterialModule,
		CommonComponentsModule,
		HelpersModule,
		RoutingModule,
		TaskModule,
	],
	exports: [ProjectTableComponent],
})
export class ProjectModule {}
