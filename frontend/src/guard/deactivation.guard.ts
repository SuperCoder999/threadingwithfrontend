import { Injectable } from "@angular/core";
import { CanDeactivate } from "@angular/router";
import { Deactivatable } from "src/models/deactivatable";

@Injectable({
	providedIn: "root",
})
export class DeactivationGuard implements CanDeactivate<Deactivatable> {
	public canDeactivate(component: Deactivatable): boolean {
		return component.canDeactivate();
	}
}
