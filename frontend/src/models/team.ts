import { User } from "./user";

export interface Team {
	id: number;
	name: string;
	createdAt: Date;
	users: User[];
}

export interface ListTeam {
	id: number;
	name: string;
	createdAt: Date;
}

export interface TeamOption {
	id: number;
	name: string;
}

export interface ModifyTeam {
	name: string;
}
