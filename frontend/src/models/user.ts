export interface User {
	id: number;
	teamId?: number;
	email: string;
	firstName?: string;
	lastName?: string;
	registeredAt: Date;
	birthDay: Date;
}

export interface UserOption {
	id: number;
	name: string;
}

export interface ModifyUser {
	teamId?: number | null;
	email: string;
	firstName?: string;
	lastName?: string;
	birthDay: Date;
}
