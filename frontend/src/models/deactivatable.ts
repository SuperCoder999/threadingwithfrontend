export interface Deactivatable {
	canDeactivate(): boolean;
}
