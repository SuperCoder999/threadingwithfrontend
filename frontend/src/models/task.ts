import { User } from "./user";

export enum TaskKanbanState {
	ToDo,
	InProgress,
	Done,
	Cancelled,
}

export interface TaskModel {
	id: number;
	projectId: number;
	performer?: User;
	name: string;
	description?: string;
	state: TaskKanbanState;
	createdAt: Date;
	finishedAt?: Date;
}

export interface TaskOption {
	id: number;
	name: string;
}

export interface ModifyTask {
	projectId: number;
	performerId?: number | null;
	name: string;
	description?: string;
	state: TaskKanbanState;
	finishedAt?: Date;
}
