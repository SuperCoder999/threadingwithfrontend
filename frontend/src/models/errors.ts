export enum ErrorMessage {
	FailedToLoad = "Failed to load.",
	NotFound = "Not found.",
	FailedToCreate = "Failed to create.",
	FailedToUpdate = "Failed to update.",
	FailedToDelete = "Failed to delete.",
}
