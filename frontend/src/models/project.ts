import { TaskModel } from "./task";
import { Team } from "./team";
import { User } from "./user";

export interface ListProject {
	id: number;
	name: string;
	description?: string;
	deadline: Date;
	createdAt: Date;
	team?: Team;
	author?: User;
}

export interface Project {
	id: number;
	name: string;
	description?: string;
	deadline: Date;
	createdAt: Date;
	team?: Team;
	author?: User;
	tasks: TaskModel[];
}

export interface ProjectOption {
	id: number;
	name: string;
}

export interface ModifyProject {
	name: string;
	description?: string;
	deadline: Date;
	teamId?: number | null;
	authorId?: number | null;
}
