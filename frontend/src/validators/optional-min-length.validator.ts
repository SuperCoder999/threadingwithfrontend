import { AbstractControl, ValidationErrors, ValidatorFn } from "@angular/forms";

export const optionalMinLengthValidator =
	(minLength: number): ValidatorFn =>
	(control: AbstractControl): ValidationErrors | null => {
		const value = String(control.value ?? "");
		const invalid = value.length > 0 ? value.length < minLength : false;

		return invalid ? { minlength: { value } } : null;
	};
