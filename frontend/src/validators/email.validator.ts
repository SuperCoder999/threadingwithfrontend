import { AbstractControl, ValidationErrors } from "@angular/forms";
import validator from "validator";

export const strictEmailValidator = (
	control: AbstractControl
): ValidationErrors | null => {
	const value = String(control.value);
	const invalid = !validator.isEmail(value);

	return invalid ? { email: { value } } : null;
};
