import { Pipe, PipeTransform } from "@angular/core";
import { formatTaskKanbanState } from "src/helpers/task-state-transform.helper";

@Pipe({
	name: "taskState",
})
export class TaskKanbanStatePipe implements PipeTransform {
	public transform(value?: number): string {
		if (value === undefined) {
			return "";
		}

		return formatTaskKanbanState(value);
	}
}
