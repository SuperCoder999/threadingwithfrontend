import { Pipe, PipeTransform } from "@angular/core";
import { formatDate } from "src/helpers/date-transform.helper";

@Pipe({
	name: "formatDate",
})
export class DateFormatPipe implements PipeTransform {
	public transform(value?: string | Date): string {
		if (!value) {
			return "";
		}

		return formatDate(new Date(value));
	}
}
