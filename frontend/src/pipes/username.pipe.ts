import { Pipe, PipeTransform } from "@angular/core";
import { formatUsername } from "src/helpers/username-transform.helper";
import { User } from "src/models/user";

@Pipe({
	name: "username",
})
export class UsernamePipe implements PipeTransform {
	public transform(value?: User): string {
		if (!value) {
			return "";
		}

		return formatUsername(value);
	}
}
