import { Directive, ElementRef, Input, OnInit } from "@angular/core";
import { getTaskStateColor } from "src/helpers/task-state.helper";
import { TaskKanbanState } from "src/models/task";

@Directive({
	selector: "[taskState]",
})
export class TaskKanbanStateDirective implements OnInit {
	@Input() public taskState!: TaskKanbanState;

	public constructor(private readonly element: ElementRef) {}

	public ngOnInit(): void {
		const color = getTaskStateColor(this.taskState);
		this.element.nativeElement.style.color = color;
	}
}
