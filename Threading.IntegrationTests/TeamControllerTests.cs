using System;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using System.Collections.Generic;
using Xunit;
using Threading.Common;
using Threading.Common.DTO.Team;
using Threading.Common.DTO.User;

namespace Threading.Integration
{
    public sealed class TeamControllerTests : IClassFixture<WebFactory>, IDisposable
    {
        private readonly HttpClient _client;

        public TeamControllerTests(WebFactory factory)
        {
            _client = factory.CreateClient();
        }

        public void Dispose()
        {
            _client.Dispose();
        }

        [Fact]
        public async Task Create_WhenAllIsValid_ThenCreate()
        {
            CreateTeamDTO data = new CreateTeamDTO { Name = "Test team" };

            HttpResponseMessage resultMessage = await _client.PostAsync(
                "api/teams",
                Utils.ConstructJsonRequestContent(data)
            );

            TeamDTO result = await Utils.DeserializeResponse<TeamDTO>(resultMessage);
            Assert.True(resultMessage.IsSuccessStatusCode);

            HttpResponseMessage actualMessage = await _client.GetAsync($"api/teams/{result.Id}");
            TeamDTO actual = await Utils.DeserializeResponse<TeamDTO>(actualMessage);

            Assert.True(actualMessage.IsSuccessStatusCode);
            Assert.Equal(result.Id, actual.Id);

            await _client.DeleteAsync($"api/teams/{result.Id}");
        }

        [Theory]
        [InlineData("")]
        public async Task Create_WhenDataIsInvalid_ThenThrow(string name)
        {
            CreateTeamDTO data = new CreateTeamDTO { Name = name };

            HttpResponseMessage resultMessage = await _client.PostAsync(
                "api/teams",
                Utils.ConstructJsonRequestContent(data)
            );

            TeamDTO result = await Utils.DeserializeResponse<TeamDTO>(resultMessage);
            Assert.False(resultMessage.IsSuccessStatusCode);
        }

        [Fact]
        public async Task GetShortWhereAllUsersOlder10_WhenAllUsersOlder10_ThenReturnTeam()
        {
            CreateTeamDTO data = new CreateTeamDTO { Name = "Test team" };

            HttpResponseMessage teamMessage = await _client.PostAsync(
                "api/teams",
                Utils.ConstructJsonRequestContent(data)
            );

            TeamDTO team = await Utils.DeserializeResponse<TeamDTO>(teamMessage);

            CreateUserDTO userData = new CreateUserDTO
            {
                FirstName = "Test",
                LastName = "Test",
                Email = "test@test.com",
                BirthDay = DateTime.UtcNow.AddYears(-30),
                TeamId = team.Id,
            };

            await _client.PostAsync(
                "api/users",
                Utils.ConstructJsonRequestContent(userData)
            );

            HttpResponseMessage actualMessage = await _client.GetAsync("api/teams/ids-names-users");
            IEnumerable<TeamShortDTO> actual = await Utils.DeserializeResponse<IEnumerable<TeamShortDTO>>(actualMessage);

            Assert.Single(actual);

            await _client.DeleteAsync($"api/teams/{team.Id}");
        }

        [Fact]
        public async Task GetShortWhereAllUsersOlder10_WhenSomeUsersYounger10_ThenDontReturnTeam()
        {
            CreateTeamDTO data = new CreateTeamDTO { Name = "Test team" };

            HttpResponseMessage teamMessage = await _client.PostAsync(
                "api/teams",
                Utils.ConstructJsonRequestContent(data)
            );

            TeamDTO team = await Utils.DeserializeResponse<TeamDTO>(teamMessage);

            CreateUserDTO userData = new CreateUserDTO
            {
                FirstName = "Test",
                LastName = "Test",
                Email = "test@test.com",
                BirthDay = DateTime.UtcNow.AddYears(-5),
                TeamId = team.Id,
            };

            await _client.PostAsync(
                "api/users",
                Utils.ConstructJsonRequestContent(userData)
            );

            HttpResponseMessage actualMessage = await _client.GetAsync("api/teams/ids-names-users");
            IEnumerable<TeamShortDTO> actual = await Utils.DeserializeResponse<IEnumerable<TeamShortDTO>>(actualMessage);

            Assert.Equal(0, actual.Count());

            await _client.DeleteAsync($"api/teams/{team.Id}");
        }

        [Fact]
        public async Task GetShortWhereAllUsersOlder10_WhenNoUsers_ThenDontReturnTeam()
        {
            CreateTeamDTO data = new CreateTeamDTO { Name = "Test team" };

            HttpResponseMessage teamMessage = await _client.PostAsync(
                "api/teams",
                Utils.ConstructJsonRequestContent(data)
            );

            TeamDTO team = await Utils.DeserializeResponse<TeamDTO>(teamMessage);

            HttpResponseMessage actualMessage = await _client.GetAsync("api/teams/ids-names-users");
            IEnumerable<TeamShortDTO> actual = await Utils.DeserializeResponse<IEnumerable<TeamShortDTO>>(actualMessage);

            Assert.Equal(0, actual.Count());

            await _client.DeleteAsync($"api/teams/{team.Id}");
        }
    }
}
