using System;
using System.Net.Http;
using System.Threading.Tasks;
using Xunit;
using Threading.Common;
using Threading.Common.DTO.User;
using Threading.Common.DTO.Team;
using Threading.Common.DTO.Project;

namespace Threading.Integration
{
    public sealed class ProjectControllerTests : IClassFixture<WebFactory>, IDisposable
    {
        private readonly HttpClient _client;

        public ProjectControllerTests(WebFactory factory)
        {
            _client = factory.CreateClient();
        }

        public void Dispose()
        {
            _client.Dispose();
        }

        [Fact]
        public async Task Create_WhenWithoutRelations_ThenCreate()
        {
            CreateProjectDTO data = new CreateProjectDTO
            {
                Name = "New project",
                Description = "Loooong descripton",
                Deadline = DateTime.UtcNow.AddDays(100),
            };

            HttpResponseMessage resultMessage = await _client.PostAsync(
                "api/projects",
                Utils.ConstructJsonRequestContent(data)
            );

            ProjectDTO result = await Utils.DeserializeResponse<ProjectDTO>(resultMessage);

            HttpResponseMessage actualMessage = await _client.GetAsync($"api/projects/{result.Id}");
            ProjectDTO actual = await Utils.DeserializeResponse<ProjectDTO>(actualMessage);

            Assert.True(actualMessage.IsSuccessStatusCode);
            Assert.Equal(result.Id, actual.Id);

            await _client.DeleteAsync($"api/projects/{result.Id}");
        }

        [Fact]
        public async Task Create_WhenWithRelations_ThenCreate()
        {
            CreateTeamDTO teamData = new CreateTeamDTO { Name = "Test team" };

            HttpResponseMessage teamMessage = await _client.PostAsync(
                "api/teams",
                Utils.ConstructJsonRequestContent(teamData)
            );

            TeamDTO team = await Utils.DeserializeResponse<TeamDTO>(teamMessage);

            CreateUserDTO userData = new CreateUserDTO
            {
                FirstName = "Test",
                LastName = "Test",
                Email = "test@test.com",
                BirthDay = DateTime.UtcNow.AddYears(-30),
                TeamId = team.Id,
            };

            HttpResponseMessage userMessage = await _client.PostAsync(
                "api/users",
                Utils.ConstructJsonRequestContent(userData)
            );

            UserDTO user = await Utils.DeserializeResponse<UserDTO>(userMessage);

            CreateProjectDTO data = new CreateProjectDTO
            {
                Name = "New project",
                Description = "Loooong descripton",
                Deadline = DateTime.UtcNow.AddDays(100),
                AuthorId = user.Id,
                TeamId = team.Id,
            };

            HttpResponseMessage resultMessage = await _client.PostAsync(
                "api/projects",
                Utils.ConstructJsonRequestContent(data)
            );

            ProjectDTO result = await Utils.DeserializeResponse<ProjectDTO>(resultMessage);
            Assert.True(resultMessage.IsSuccessStatusCode);

            HttpResponseMessage actualMessage = await _client.GetAsync($"api/projects/{result.Id}");
            ProjectDTO actual = await Utils.DeserializeResponse<ProjectDTO>(actualMessage);

            Assert.True(actualMessage.IsSuccessStatusCode);
            Assert.Equal(result.Id, actual.Id);
            Assert.NotNull(actual.Team);
            Assert.Equal(result.Team.Value.Id, actual.Team.Value.Id);
            Assert.NotNull(actual.Author);
            Assert.Equal(result.Author.Value.Id, actual.Author.Value.Id);

            await _client.DeleteAsync($"api/projects/{result.Id}");
        }

        [Theory]
        [InlineData("", "Loooong descripton", null, null)]
        [InlineData("New project", "", null, null)]
        [InlineData("New project", "Loooong descripton", 99, null)]
        [InlineData("New project", "Loooong descripton", null, 99)]
        public async Task Create_WhenBodyIsInvalid_ThenThrow(string name, string description, int? author, int? team)
        {
            CreateProjectDTO data = new CreateProjectDTO
            {
                Name = name,
                Description = description,
                Deadline = DateTime.UtcNow.AddDays(100),
                AuthorId = author,
                TeamId = team,
            };

            HttpResponseMessage resultMessage = await _client.PostAsync(
                "api/projects",
                Utils.ConstructJsonRequestContent(data)
            );

            Assert.False(resultMessage.IsSuccessStatusCode);
        }
    }
}
