using System;
using System.Net.Http;
using System.Threading.Tasks;
using Xunit;
using Threading.Common;
using Threading.Common.DTO.User;

namespace Threading.Integration
{
    public sealed class UserControllerTests : IClassFixture<WebFactory>, IDisposable
    {
        private readonly HttpClient _client;

        public UserControllerTests(WebFactory factory)
        {
            _client = factory.CreateClient();
        }

        public void Dispose()
        {
            _client.Dispose();
        }

        [Fact]
        public async Task Delete_WhenAllIsRight_ThenDelete()
        {
            CreateUserDTO userData = new CreateUserDTO
            {
                FirstName = "Test",
                LastName = "Test",
                Email = "test@test.com",
                BirthDay = DateTime.UtcNow.AddYears(-30),
            };

            HttpResponseMessage userMessage = await _client.PostAsync(
                "api/users",
                Utils.ConstructJsonRequestContent(userData)
            );

            UserDTO user = await Utils.DeserializeResponse<UserDTO>(userMessage);

            HttpResponseMessage deleteMessage = await _client.DeleteAsync($"api/users/{user.Id}");
            Assert.True(deleteMessage.IsSuccessStatusCode);

            HttpResponseMessage checkMessage = await _client.GetAsync($"api/users/{user.Id}");
            Assert.False(checkMessage.IsSuccessStatusCode);
        }

        [Fact]
        public async Task Delete_WhenUnexistingUser_ThenThrow()
        {
            HttpResponseMessage deleteMessage = await _client.DeleteAsync("api/users/99");
            Assert.False(deleteMessage.IsSuccessStatusCode);
        }
    }
}
