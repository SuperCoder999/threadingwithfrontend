# Threading

## Requirements:

- .NET 5.0
- Docker
- docker-compose
- nodejs
- npm

## Setup:

### Backend:

- Create `.env` and `.database.env` based on `.env.example` and `.database.env.example`
- `docker-compose up -d`
- `cd Threading.DAL`
- `dotnet ef database update --startup-project ../Threading.WebAPI`

### Frontend:

- `cd frontend`
- `npm install`

## Start:

### Backend:

- `cd Threading.WebAPI`
- `dotnet run`

### Frontend:

- `cd frontend`
- `npm start`

## DB operations:

- `cd Threading.DAL`
- `dotnet ef <something> --startup-project ../Threading.WebAPI`

### If `dotnet ef <something> --startup-project ../Threading.WebAPI` fails (can't connect to DB, etc.)

Uncomment marked lines in `Threading.DAL/Context/DatabaseContext.cs`

## Notes:

- Business logic is located at `Threading.BLL`
- Unit tests are located at `Threading.Unit`
- Integration tests are located at `Threading.Integration`
- PostgreSQL is used, because it's faster
- First DB queries are automatically cached, so may last more time and load more data
- Create and Update pages are modals
- Delete is with confirmation
- Cyan elements are links
- I've added pages for one entity and sorting in tables
