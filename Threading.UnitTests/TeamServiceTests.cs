using System;
using System.Linq;
using System.Threading.Tasks;
using System.Collections.Generic;
using Xunit;
using AutoMapper;
using Threading.BLL.Interfaces;
using Threading.BLL.Services;
using Threading.BLL.Validators.Team;
using Threading.DAL.Entities;
using Threading.DAL.Context;
using Threading.Common.DTO.Team;

namespace Threading.Unit
{
    public sealed class TeamServiceTests : IDisposable
    {
        private readonly IMapper _mapper;
        private readonly DatabaseContext _context;
        private readonly ITeamService _teamService;
        private const int BASIC_USERS_COUNT = 3;

        public TeamServiceTests()
        {
            (_mapper, _context, _, _) = TestUtils.GenerateCommonServiceArgs();

            _teamService = new TeamService(
                _context,
                _mapper,
                new CreateTeamValidator(),
                new UpdateTeamValidator()
            );
        }

        public void Dispose()
        {
            _context.Database.EnsureDeleted();
            _context.Dispose();
        }

        [Fact]
        public async Task GetShortWithUsersOlder10_WhenAllUsersOlder10_ThenReturnTeam()
        {
            var (team, users) = await SeedBasicData(true);

            IEnumerable<TeamShortDTO> actualList = await _teamService.GetShortWithUsersOlder10Async();
            Assert.Single(actualList);

            TeamShortDTO actual = actualList.First();

            Assert.Equal(team.Id, actual.Id);
            Assert.Equal(BASIC_USERS_COUNT, actual.Users.Count());
        }

        [Fact]
        public async Task GetShortWithUsersOlder10_WhenSomeUsersYounger10_ThenDontReturnTeam()
        {
            var (team, users) = await SeedBasicData();

            IEnumerable<TeamShortDTO> actualList = await _teamService.GetShortWithUsersOlder10Async();
            Assert.Equal(0, actualList.Count());
        }

        [Fact]
        public async Task GetShortWithUsersOlder10_WhenNoUsers_ThenDontReturnTeam()
        {
            Team team = new Team { Name = "Test team" };
            _context.Teams.Add(team);
            await _context.SaveChangesAsync();

            IEnumerable<TeamShortDTO> actualList = await _teamService.GetShortWithUsersOlder10Async();
            Assert.Equal(0, actualList.Count());
        }

        private async Task<(Team, IList<User>)> SeedBasicData(bool allUsersOlder10 = false)
        {
            Team team = new Team { Name = "Test team" };
            _context.Teams.Add(team);

            List<User> users = new List<User>();

            if (allUsersOlder10)
            {
                for (int i = 0; i < BASIC_USERS_COUNT; i++)
                {
                    User user = new User
                    {
                        Email = "test@test.com",
                        FirstName = "Test",
                        LastName = "Test",
                        TeamId = team.Id,
                        BirthDay = DateTime.UtcNow.AddYears(-11),
                    };

                    _context.Users.Add(user);
                    users.Add(user);
                }
            }
            else
            {
                User firstUser = new User
                {
                    Email = "test@test.com",
                    FirstName = "Test",
                    LastName = "Test",
                    TeamId = team.Id,
                    BirthDay = DateTime.UtcNow.AddYears(-11),
                };

                _context.Users.Add(firstUser);
                users.Add(firstUser);

                for (int i = 0; i < BASIC_USERS_COUNT; i++)
                {
                    User user = new User
                    {
                        Email = "test@test.com",
                        FirstName = "Test",
                        LastName = "Test",
                        TeamId = team.Id,
                        BirthDay = DateTime.UtcNow.AddYears(-9),
                    };

                    _context.Users.Add(user);
                    users.Add(user);
                }
            }

            await _context.SaveChangesAsync();
            return (team, users);
        }
    }
}
