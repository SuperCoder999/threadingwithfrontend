using System;
using dotenv.net;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.EntityFrameworkCore;
using Microsoft.OpenApi.Models;
using FluentValidation;
using FluentValidation.AspNetCore;
using Threading.DAL.Context;
using Threading.BLL.Interfaces;
using Threading.BLL.Services;
using Threading.BLL.MappingProfiles;
using Threading.BLL.Validators.User;
using Threading.BLL.Validators.Task;
using Threading.BLL.Validators.Team;
using Threading.BLL.Validators.Project;
using Threading.Common.DTO.User;
using Threading.Common.DTO.Task;
using Threading.Common.DTO.Team;
using Threading.Common.DTO.Project;
using Threading.WebAPI.Filters;

namespace Threading.WebAPI.Extensions
{
    public static class ServiceExtensions
    {
        public static void LoadEnvironment(this IServiceCollection services)
        {
            DotEnv.Load(new DotEnvOptions(false, new string[] { "../.env" }));
        }

        public static void AddDAL(this IServiceCollection services)
        {
            services.AddDbContext<DatabaseContext>(opt =>
                DatabaseContext.ConfigurePostgreSQL(opt));
        }

        public static void AddThreadingDAL(this IServiceCollection services)
        {
            services.AddDbContext<DatabaseContext>(opt => opt
                .UseInMemoryDatabase("TestDB"));
        }

        public static void AddMapper(this IServiceCollection services)
        {
            services.AddAutoMapper(opt =>
            {
                opt.AddProfile<UserProfile>();
                opt.AddProfile<TaskProfile>();
                opt.AddProfile<TeamProfile>();
                opt.AddProfile<ProjectProfile>();
            });
        }

        public static void AddValidation(this IServiceCollection services)
        {
            services.AddSingleton<IValidator<CreateUserDTO>, CreateUserValidator>();
            services.AddSingleton<IValidator<UpdateUserDTO>, UpdateUserValidator>();

            services.AddSingleton<IValidator<CreateTaskDTO>, CreateTaskValidator>();
            services.AddSingleton<IValidator<UpdateTaskDTO>, UpdateTaskValidator>();

            services.AddSingleton<IValidator<CreateTeamDTO>, CreateTeamValidator>();
            services.AddSingleton<IValidator<UpdateTeamDTO>, UpdateTeamValidator>();

            services.AddSingleton<IValidator<CreateProjectDTO>, CreateProjectValidator>();
            services.AddSingleton<IValidator<UpdateProjectDTO>, UpdateProjectValidator>();
        }

        public static void AddBLL(this IServiceCollection services)
        {
            services.AddScoped<ITeamService, TeamService>();
            services.AddScoped<IUserService, UserService>();
            services.AddScoped<IProjectService, ProjectService>();
            services.AddScoped<ITaskService, TaskService>();
        }

        public static void AddSwagger(this IServiceCollection services)
        {
            services.AddSwaggerGen(opt =>
            {
                opt.SwaggerDoc("v1", new OpenApiInfo
                {
                    Title = "Threading API",
                    Version = "1.0.0"
                });
            });
        }

        public static void AddAPI(this IServiceCollection services)
        {
            services
                .AddControllers(opt => opt.Filters.Add(typeof(CustomExceptionFilterAttribute)))
                .AddNewtonsoftJson()
                .AddFluentValidation();
        }

        public static void ConfigureDevelopmentServices(this IServiceCollection services)
        {
            services.LoadEnvironment();
            services.AddDAL();
            services.AddMapper();
            services.AddValidation();
            services.AddBLL();
            services.AddCors();
            services.AddAPI();
            services.AddSwagger();
        }

        public static void ConfigureThreadingServices(this IServiceCollection services)
        {
            services.AddThreadingDAL();
            services.AddMapper();
            services.AddValidation();
            services.AddBLL();
            services.AddCors();
            services.AddAPI();
            services.AddSwagger();
        }
    }
}
