using System.Threading.Tasks;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using Threading.DAL.Entities;
using Threading.BLL.Interfaces;
using Threading.Common.DTO.User;

namespace Threading.WebAPI.Controllers
{
    [ApiController]
    [Route("api/users")]
    public sealed class UserController
        : AbstractController<IUserService, User, UserDTO, UserOptionDTO, UserDTO, CreateUserDTO, UpdateUserDTO>
    {
        public UserController(IUserService service) : base(service) { }

        [HttpGet("with-tasks")]
        public async Task<ActionResult<List<UserWithTasksDTO>>> GetWithTasksOrderedByFirstName()
        {
            return Ok(await _service.GetWithTasksOrderedByFirstNameAsync());
        }

        [HttpGet("{id}/additional-info")]
        public async Task<ActionResult<UserAdditionalInfoDTO>> GetAdditionalInfo([FromRoute] int id)
        {
            return Ok(await _service.GetAdditionalInfoAsync(id));
        }
    }
}
