using System.IO;
using System.Text;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Threading.DAL.Entities;
using Threading.BLL.Interfaces;

namespace Threading.WebAPI.Controllers
{
    public abstract class AbstractController<TService, TEntity, TListDTO, TOptionDTO, TDTO, TCreateDTO, TUpdateDTO> : ControllerBase
        where TService : IService<TEntity, TListDTO, TOptionDTO, TDTO, TCreateDTO>
        where TEntity : class, IEntity, new()
        where TDTO : struct
        where TCreateDTO : struct
        where TUpdateDTO : struct
    {
        protected readonly TService _service;

        public AbstractController(TService service)
        {
            _service = service;
        }

        [HttpGet]
        public virtual async Task<ActionResult<List<TListDTO>>> Get()
        {
            return Ok(await _service.GetAsync());
        }

        [HttpGet("{id}")]
        public virtual async Task<ActionResult<TDTO>> Get([FromRoute] int id)
        {
            return Ok(await _service.GetAsync(id));
        }

        [HttpGet("options")]
        public virtual async Task<ActionResult<List<TOptionDTO>>> GetOptions()
        {
            return Ok(await _service.GetOptionsAsync());
        }

        [HttpPost]
        public virtual async Task<ActionResult<TDTO>> Create([FromBody] TCreateDTO data)
        {
            int id = await _service.CreateAsync(data);
            return Created(nameof(TEntity), await _service.GetAsync(id));
        }

        [HttpPatch("{id}")]
        public virtual async Task<ActionResult<TDTO>> Update([FromRoute] int id)
        {
            string stringBody = await ParseStringRequestBody();

            TEntity entity = await _service.GetRawAsync(id);
            entity = await _service.RepopulateUpdateRequestAsync(stringBody, entity);
            await _service.UpdateAsync(id, entity);

            return Ok(await _service.GetAsync(id));
        }

        [HttpDelete("{id}")]
        public virtual async Task<ActionResult> Delete([FromRoute] int id)
        {
            await _service.DeleteAsync(id);
            return NoContent();
        }

        protected virtual async Task<string> ParseStringRequestBody()
        {
            using (StreamReader reader = new StreamReader(Request.Body, Encoding.UTF8))
            {
                return await reader.ReadToEndAsync();
            }
        }
    }
}
