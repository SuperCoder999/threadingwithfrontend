using System.Threading.Tasks;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using Threading.DAL.Entities;
using Threading.BLL.Interfaces;
using Threading.Common.DTO.Project;

namespace Threading.WebAPI.Controllers
{
    [ApiController]
    [Route("api/projects")]
    public sealed class ProjectController
        : AbstractController<IProjectService, Project, ListProjectDTO, ProjectOptionDTO, ProjectDTO, CreateProjectDTO, UpdateProjectDTO>
    {
        public ProjectController(IProjectService service) : base(service) { }

        [HttpGet("with-tasks-count/{userId}")]
        public async Task<ActionResult<List<ProjectWithTasksCountDTO>>> GetWithTasksCount([FromRoute] int userId)
        {
            return Ok(await _service.GetWithTasksCountAsync(userId));
        }

        [HttpGet("{id}/additional-info")]
        public async Task<ActionResult<ProjectAdditionalInfoDTO>> GetAdditionalInfo([FromRoute] int id)
        {
            return Ok(await _service.GetAdditionalInfoAsync(id));
        }
    }
}
