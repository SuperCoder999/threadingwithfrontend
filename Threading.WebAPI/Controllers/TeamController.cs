using System.Threading.Tasks;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using Threading.DAL.Entities;
using Threading.BLL.Interfaces;
using Threading.Common.DTO.Team;

namespace Threading.WebAPI.Controllers
{
    [ApiController]
    [Route("api/teams")]
    public sealed class TeamController
        : AbstractController<ITeamService, Team, ListTeamDTO, TeamOptionDTO, TeamDTO, CreateTeamDTO, UpdateTeamDTO>
    {
        public TeamController(ITeamService service) : base(service) { }

        [HttpGet("ids-names-users")]
        public async Task<ActionResult<List<TeamShortDTO>>> GetShortWithUsersOlder10()
        {
            return Ok(await _service.GetShortWithUsersOlder10Async());
        }
    }
}
