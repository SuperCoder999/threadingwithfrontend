using System.Threading.Tasks;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using Threading.DAL.Entities;
using Threading.BLL.Interfaces;
using Threading.Common.DTO.Task;

namespace Threading.WebAPI.Controllers
{
    [ApiController]
    [Route("api/tasks")]
    public sealed class TaskController
        : AbstractController<ITaskService, TaskModel, TaskDTO, TaskOptionDTO, TaskDTO, CreateTaskDTO, UpdateTaskDTO>
    {
        public TaskController(ITaskService service) : base(service) { }

        [HttpGet("with-short-name/{userId}")]
        public async Task<ActionResult<List<TaskDTO>>> GetAssignedToWithShortName([FromRoute] int userId)
        {
            return Ok(await _service.GetAssignedToWithShortNameAsync(userId));
        }

        [HttpGet("finished-in-current-year/{userId}")]
        public async Task<ActionResult<List<TaskShortDTO>>> GetShortFinishedInCurrentYearAssignedTo([FromRoute] int userId)
        {
            return Ok(await _service.GetShortFinishedInCurrentYearAssignedToAsync(userId));
        }

        [HttpGet("unfinished")]
        public async Task<ActionResult<List<TaskDTO>>> GetUnfinished()
        {
            return Ok(await _service.GetUnfinishedAsync());
        }

        [HttpGet("unfinished/{userId}")]
        public async Task<ActionResult<List<TaskWithoutPerformerDTO>>> GetUnfinishedFor([FromRoute] int userId)
        {
            return Ok(await _service.GetUnfinishedForAsync(userId));
        }
    }
}
