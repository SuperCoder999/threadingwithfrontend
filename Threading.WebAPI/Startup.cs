using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Threading.WebAPI.Extensions;

namespace Threading.WebAPI
{
    public sealed class Startup
    {
        public void ConfigureServices(IServiceCollection services)
        {
            services.ConfigureDevelopmentServices();
        }

        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseHsts();
            }

            app.UseCors(opt => opt
                .AllowAnyHeader()
                .AllowAnyMethod()
                .WithOrigins("http://localhost:3000", "http://localhost:4200"));

            app.UseRouting();
            app.UseHttpsRedirection();
            app.UseEndpoints(endpoints => endpoints.MapControllers());

            app.UseSwagger();
            app.UseSwaggerUI(opt => opt.SwaggerEndpoint("/swagger/v1/swagger.json", "Threading API"));
        }
    }
}
