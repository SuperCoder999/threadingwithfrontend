using System.Collections.Generic;
using Threading.DAL.Entities;

namespace Threading.DAL.Seeds
{
    internal interface ISeeder
    {
        IEnumerable<User> GetUsers(int count, IEnumerable<Team> teams);
        IEnumerable<TaskModel> GetTasks(int count, IEnumerable<User> users, IEnumerable<Project> projects);
        IEnumerable<Team> GetTeams(int count);
        IEnumerable<Project> GetProjects(int count, IEnumerable<User> users, IEnumerable<Team> teams);
    }
}
