using System;
using Microsoft.EntityFrameworkCore;
using Threading.DAL.Entities;

namespace Threading.DAL.Context
{
    public class DatabaseContext : DbContext
    {
        private readonly bool _seed = true;
        public virtual DbSet<User> Users { get; set; }
        public virtual DbSet<TaskModel> Tasks { get; set; }
        public virtual DbSet<Team> Teams { get; set; }
        public virtual DbSet<Project> Projects { get; set; }
        public DbContextOptions<DatabaseContext> Options { get; set; }

        public DatabaseContext(bool seed = true) : base()
        {
            _seed = seed;
        }

        public DatabaseContext(DbContextOptions<DatabaseContext> opt, bool seed = true) : base(opt)
        {
            _seed = seed;
            Options = opt;
        }

        public static DbContextOptionsBuilder ConfigurePostgreSQL(DbContextOptionsBuilder opt)
        {
            string migrationsAssembly = typeof(DatabaseContext).Assembly.GetName().FullName;

            return opt.UseNpgsql(
                Environment.GetEnvironmentVariable("DB_CONNECTION_STRING"),
                opt => opt
                    .MigrationsAssembly(migrationsAssembly)
                    .UseQuerySplittingBehavior(QuerySplittingBehavior.SingleQuery)
            );
        }

        protected override void OnModelCreating(ModelBuilder opt)
        {
            opt.ConfigureModels();

            if (_seed)
            {
                opt.Seed();
            }

            base.OnModelCreating(opt);
        }

        /* Temporary uncomment if `dotnet ef <something>` fails */
        /*
        protected override void OnConfiguring(DbContextOptionsBuilder opt)
        {
            if (!opt.IsConfigured)
            {
                ConfigurePostgreSQL(opt);
            }
        }
        */
    }
}
