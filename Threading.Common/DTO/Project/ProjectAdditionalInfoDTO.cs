using Threading.Common.DTO.Task;

namespace Threading.Common.DTO.Project
{
    public struct ProjectAdditionalInfoDTO
    {
        public ProjectDTO Project { get; set; }
        public TaskDTO? LongestDescriptionTask { get; set; }
        public TaskDTO? ShortestNameTask { get; set; }
        public int? UsersCountInTeam { get; set; }
    }
}
