using System;
using System.Collections.Generic;
using Threading.Common.DTO.Team;
using Threading.Common.DTO.Task;
using Threading.Common.DTO.User;

namespace Threading.Common.DTO.Project
{
    public struct ProjectWithTasksCountDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public DateTime Deadline { get; set; }
        public DateTime CreatedAt { get; set; }

        public IEnumerable<TaskDTO> Tasks;
        public TeamDTO? Team { get; set; }
        public UserDTO? Author { get; set; }
        public int TasksCount { get; set; }
    }
}
