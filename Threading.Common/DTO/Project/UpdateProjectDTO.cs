#nullable enable

using System;

namespace Threading.Common.DTO.Project
{
    public struct UpdateProjectDTO
    {
        public int? AuthorId { get; set; }
        public int? TeamId { get; set; }
        public string? Name { get; set; }
        public string? Description { get; set; }
        public DateTime? Deadline { get; set; }
    }
}
