using System;
using Threading.Common.DTO.Team;
using Threading.Common.DTO.User;

namespace Threading.Common.DTO.Project
{
    public struct ListProjectDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public DateTime Deadline { get; set; }
        public DateTime CreatedAt { get; set; }

        public ListTeamDTO? Team { get; set; }
        public UserDTO? Author { get; set; }
    }
}
