namespace Threading.Common.DTO.User
{
    public struct UserOptionDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
