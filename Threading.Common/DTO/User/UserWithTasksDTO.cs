using System;
using System.Collections.Generic;
using Threading.Common.DTO.Task;

namespace Threading.Common.DTO.User
{
    public struct UserWithTasksDTO
    {
        public int Id { get; set; }
        public int? TeamId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public DateTime RegisteredAt { get; set; }
        public DateTime BirthDay { get; set; }
        public IEnumerable<TaskWithoutPerformerDTO> Tasks { get; set; }
    }
}
