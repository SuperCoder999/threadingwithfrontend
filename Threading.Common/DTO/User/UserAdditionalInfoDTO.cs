using Threading.Common.DTO.Project;
using Threading.Common.DTO.Task;

namespace Threading.Common.DTO.User
{
    public struct UserAdditionalInfoDTO
    {
        public UserDTO User { get; set; }
        public ProjectDTO? LastProject { get; set; }
        public int LastProjectTasksCount { get; set; }
        public int UnfinishedTasksCount { get; set; }
        public TaskWithoutPerformerDTO? LongestTask { get; set; }
    }
}
