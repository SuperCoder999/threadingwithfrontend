namespace Threading.Common.DTO.Team
{
    public struct TeamOptionDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
