using Newtonsoft.Json;

namespace Threading.Common.DTO.Team
{
    public struct CreateTeamDTO
    {
        [JsonRequired] public string Name { get; set; }
    }
}
