using System;

namespace Threading.Common.DTO.Team
{
    public struct ListTeamDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public DateTime CreatedAt { get; set; }
    }
}
