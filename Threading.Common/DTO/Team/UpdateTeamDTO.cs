#nullable enable

namespace Threading.Common.DTO.Team
{
    public struct UpdateTeamDTO
    {
        public string? Name { get; set; }
    }
}
