using System.Collections.Generic;
using Threading.Common.DTO.User;

namespace Threading.Common.DTO.Team
{
    public struct TeamShortDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public IEnumerable<UserDTO> Users;
    }
}
