using System;
using System.Collections.Generic;
using Threading.Common.DTO.User;

namespace Threading.Common.DTO.Team
{
    public struct TeamDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public DateTime CreatedAt { get; set; }

        public IEnumerable<UserDTO> Users;
    }
}
