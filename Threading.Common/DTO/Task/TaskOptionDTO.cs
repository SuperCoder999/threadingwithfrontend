namespace Threading.Common.DTO.Task
{
    public struct TaskOptionDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
