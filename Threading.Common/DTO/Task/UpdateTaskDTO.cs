#nullable enable

using System;
using Threading.Common.Enums;

namespace Threading.Common.DTO.Task
{
    public struct UpdateTaskDTO
    {
        public int? ProjectId { get; set; }
        public int? PerformerId { get; set; }
        public string? Name { get; set; }
        public string? Description { get; set; }
        public TaskState? State { get; set; }
        public DateTime? FinishedAt { get; set; }
    }
}
