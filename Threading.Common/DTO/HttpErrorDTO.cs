namespace Threading.Common.DTO
{
    public struct HttpErrorDTO
    {
        public string Error { get; set; }
    }
}
