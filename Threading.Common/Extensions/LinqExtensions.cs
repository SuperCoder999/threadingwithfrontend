using System.Linq;
using System.Threading.Tasks;
using System.Collections.Generic;

namespace Threading.Common.Extensions
{
    public static class LinqExtensions
    {
        public static async Task<List<T>> ToListAsync<T>(this IEnumerable<T> collection)
        {
            return await Task.Run(collection.ToList);
        }
    }
}
