namespace Threading.Common.Enums
{
    public enum TaskState
    {
        ToDo,
        InProgress,
        Done,
        Cancelled,
    }
}
